#!/usr/bin/env python3
import os, sys, numpy, pandas
import matplotlib as mplot


config = dict()
config['general'] = dict()
config['general']['generated_data'] = '/home/jonathan/kaggle/belkin/generatedData/'


house = 1
file_path = config['general']['generated_data'] + 'multiTrainSetH' + str(house) + '.csv'
multi_train_set = pandas.read_csv(file_path)
multi_train_set