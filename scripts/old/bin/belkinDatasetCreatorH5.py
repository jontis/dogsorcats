import numpy as np
import os, sys, random, pandas # pickle, gzip, 

from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix

class BelkinDataset(DenseDesignMatrix):
	
	def __init__(self, house, set_type=None, set_which='train', y_type='normal', preprocessor=None, drop_feats=None, raw=False):
		""" set_type: None for bulk data, tt for train / test split, tvt for train / valid / test
		"""
		self.house = house = str(house)
		self.raw = raw
		print 'processing house ' + self.house
		self.data_path = '/home/jonathan/kaggle/belkin/generatedData/'
		if raw: data_file = 'train_multi_H' + self.house + '.h5'
		else: data_file = 'train_multi_H' + self.house + '-full.h5'
		# create data sets on file
		self.CheckForSetsOnFile(house)
		if not self.sets_on_file['tt']: self.CreateSetTT(data_file)
		if not self.sets_on_file['tvt']: self.CreateSetTVT(data_file)
		
		# load sets
		if (set_type == 'tt'): data_set = self.LoadSetTT(set_which)
		elif (set_type == 'tvt'): data_set = self.LoadSetTVT(set_which)
		elif (set_type == 'submission'): data_set = self.LoadSetSub(raw)
		else: data_set = pandas.read_hdf(self.data_path + data_file, 'df')
		
		# drop feats
		if drop_feats is not None: 
			print 'dropping feats ' + drop_feats
			data_set = self.DropCols(data_set, drop_feats)
		
		self.t = data_set.pop('t') # store ticks somewhere safe, they don't generalize
		if (set_type != 'submission'): 
			y = data_set.pop('label').values
			data_ids = data_set.pop('id').values
			one_hot = np.zeros((len(y), max(y)+1))
			for i in xrange(len(y)): one_hot[i, y[i]] = 1
		else:
			y = None
		X = data_set.values
		
		if y_type == 'one_hot':
			super(BelkinDataset, self).__init__(X=X, y=one_hot, preprocessor=preprocessor)
		else:
			super(BelkinDataset, self).__init__(X=X, y=y, preprocessor=preprocessor)
	
	
	def LoadSetTT(self, set_which):
		data_file = 'train_multi_H' + self.house + '-tt.h5'
		print 'loading ' + data_file
		return pandas.read_hdf(self.data_path + data_file, set_which)
	
	
	def LoadSetTVT(self, set_which):
		data_file = 'train_multi_H' + self.house + '-tvt.h5'
		print 'loading ' + data_file
		return pandas.read_hdf(self.data_path + data_file, set_which)
	
	
	def LoadSetSub(self, raw):
		if raw: data_file = 'test_H' + self.house + '.h5'
		else: data_file = 'test_H' + self.house + '-full.h5'
		print 'loading ' + data_file
		return pandas.read_hdf(self.data_path + data_file, 'df')
	
	
	def CreateSetTT(self, data_file):
		print 'creating set tt'
		data_set = pandas.read_hdf(self.data_path + data_file, 'df')
		train_share = 0.7
		train_index = random.sample(range(data_set.shape[0]), int(train_share*data_set.shape[0]))
		train_set = data_set.iloc[train_index]
		test_set = data_set.drop(train_set.index)
		data_file = 'train_multi_H' + self.house + '-tt.h5'
		train_set.to_hdf(self.data_path + data_file, 'train', append=False)
		test_set.to_hdf(self.data_path + data_file, 'test', append=False)
	
	
	def CreateSetTVT(self, data_file):
		print 'creating set tvt'
		data_set = pandas.read_hdf(self.data_path + data_file, 'df')
		train_share = 0.6
		valid_test_split = 0.5		
		train_index = random.sample(range(data_set.shape[0]), int(train_share*data_set.shape[0])) 
		train_set = data_set.iloc[train_index]
		rest_set = data_set.drop(train_set.index)
		valid_index = random.sample(range(rest_set.shape[0]), int(valid_test_split*rest_set.shape[0]))
		valid_set = rest_set.iloc[valid_index]
		test_set = rest_set.drop(valid_set.index)
		data_file = 'train_multi_H' + self.house + '-tvt.h5'
		train_set.to_hdf(self.data_path + data_file, 'train', append=False)
		train_set.to_hdf(self.data_path + data_file, 'valid', append=False)
		train_set.to_hdf(self.data_path + data_file, 'test', append=False)
	
	
	def CheckForSetsOnFile(self, house):
		self.sets_on_file = {}
		self.sets_on_file['tt'] = os.path.isfile(self.data_path + 'train_multi_H' + house + '-tt.h5')
		self.sets_on_file['tvt'] = os.path.isfile(self.data_path + 'train_multi_H' + house + '-tvt.h5')
	
	
	def DropCols(self, data_set, drop_feats):
		time_cols = ['hour']
		date_cols = ['month', 'wday']
		lf_cols = ['l12_pf', 'l1_pf', 'l2_pf', 'p12_app', 'p12_im', 'p12_re', 'p1_app', 'p1_im', 'p1_re', 'p2_app', 'p2_im', 'p2_re']
		hf_cols = [i for i in range(4096)]
		drop_cols = []
		if 'time' in drop_feats: drop_cols += time_cols
		if 'date' in drop_feats: drop_cols += date_cols
		if 'lf' in drop_feats: drop_cols += lf_cols
		if 'hf' in drop_feats: drop_cols += hf_cols
		data_set = data_set.drop(drop_cols, 1)
		return data_set
