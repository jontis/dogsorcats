import numpy as np
import os, sys, pickle, gzip, random

from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix

class BelkinDataset(DenseDesignMatrix):
	
	def __init__(self, set_type=None, set_which='train', y_type='one_hot', preprocessor=None):
		""" set_type: None for bulk data, tt for train / test split, tvt for train / valid / test
		"""
		# create data sets on file
		self.CheckForSetsOnFile()
		if not (self.sets_on_file['tt'] & self.sets_on_file['tvt']): data_set = self.LoadGzPickle('multiTrainSetH1-py2-full.pkl.gz')
		if not self.sets_on_file['tt']: self.CreateSetTT(data_set)
		if not self.sets_on_file['tvt']: self.CreateSetTVT(data_set)
		
		# load sets
		if (set_type == 'tt'): (X, y) = self.LoadSetTT(set_which)
		elif (set_type == 'tvt'): (X, y) = self.LoadSetTVT(set_which)
		else: (X, y) = self.LoadGzPickle('multiTrainSetH1-py2-full.pkl.gz')

		one_hot = np.zeros((len(y), max(y)+1))
		for i in xrange(len(y)): one_hot[i, y[i]] = 1
		
		if y_type == 'one_hot':
			super(BelkinDataset, self).__init__(X=X, y=one_hot, preprocessor=preprocessor)
		else:
			super(BelkinDataset, self).__init__(X=X, y=y, preprocessor=preprocessor)
			
	
	def LoadSetTT(self, set_which):
		data_file = 'multiTrainSetH1-py2-tt.pkl.gz'
		data_sets = self.LoadGzPickle(data_file)
		if set_which == 'train': return data_sets[0]
		else: return data_sets[1]

	def LoadSetTVT(self, set_which):
		data_file = 'multiTrainSetH1-py2-tvt.pkl.gz'
		data_sets = self.LoadGzPickle(data_file)
		if set_which == 'train': return data_sets[0]
		elif set_which == 'valid': return data_sets[1]
		else: return data_sets[2]

		
	def CreateSetTT(self, data_set):
		train_share = 0.7
		train_index = random.sample(range(data_set[0].shape[0]), int(train_share*data_set[0].shape[0])) 
		train_set = (data_set[0][train_index], data_set[1][train_index])
		test_set = (np.delete(data_set[0], train_index, 0), np.delete(data_set[1], train_index, 0))
		data_file = 'multiTrainSetH1-py2-tt.pkl.gz'
		self.SaveGzPickle(data_file, [train_set, test_set])


	def CreateSetTVT(self, data_set):
		train_share = 0.6
		valid_test_split = 0.5
		train_index = random.sample(range(data_set[0].shape[0]), int(train_share*data_set[0].shape[0])) 
		train_set = (data_set[0][train_index], data_set[1][train_index])
		rest_set = (np.delete(data_set[0], train_index, 0), np.delete(data_set[1], train_index, 0))
		valid_index = random.sample(range(rest_set[0].shape[0]), int(valid_test_split*rest_set[0].shape[0]))
		valid_set = (rest_set[0][valid_index], rest_set[1][valid_index])
		test_set = (np.delete(rest_set[0], valid_index, 0), np.delete(rest_set[1], valid_index, 0))
		data_file = 'multiTrainSetH1-py2-tvt.pkl.gz'
		self.SaveGzPickle(data_file, [train_set, valid_set, test_set])	
		 
		
		
	def LoadGzPickle(self, data_file):
		f = gzip.open(data_file, 'rb')
		data_sets = pickle.load(f)
		f.close()
		return data_sets

		
	def SaveGzPickle(self, data_file, data_sets):
		f = gzip.open(data_file, 'wb')
		pickle.dump(data_sets, f)
		f.close()


	def CheckForSetsOnFile(self):
		self.sets_on_file = {}
		self.sets_on_file['full'] = os.path.isfile('multiTrainSetH1-py2-full.pkl.gz')
		self.sets_on_file['tt'] = os.path.isfile('multiTrainSetH1-py2-tt.pkl.gz')
		self.sets_on_file['tvt'] = os.path.isfile('multiTrainSetH1-py2-tvt.pkl.gz')
