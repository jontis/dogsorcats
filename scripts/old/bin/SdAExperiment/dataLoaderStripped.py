import pandas, random, gzip, pickle, numpy
from sklearn.preprocessing import scale

def DataLoaderStripped(house):
	house = str(house)
	print 'processing house ' + house
	data_path = '/home/jonathan/kaggle/belkin/generatedData/'
	all_data = pandas.read_csv(data_path + 'multiTrainSetH' + house + '.csv')
	all_data = all_data.drop(['Unnamed: 0'], axis=1)
	
	# preprocess, scenter and scale
	all_data_labels = all_data.pop('class').values
	all_data = all_data.drop('ticks',1) # drop ticks, they are a too good identifier so the models does not generalize
	all_data = pandas.DataFrame(scale(all_data), index=all_data.index, columns=all_data.columns)
	all_data['class'] = all_data_labels
	
	# pickle without compression, fast but spacy
	#f = open(, 'wb')
	#pickle.dump(datasets, f) 
	#f.close()
	
	#f = gzip.open(data_path + 'multiTrainSetH' + house + '-py2.pkl.gz', 'wb') # gzip is slow
	#pickle.dump(all_data, f) 
	#f.close()
	
	# persist to hdf, fast and small
	all_data.to_hdf(data_path + 'multiTrainSetH' + house + '.h5', 'table')

DataLoaderStripped(1)
DataLoaderStripped(2)
DataLoaderStripped(3)
DataLoaderStripped(4)

