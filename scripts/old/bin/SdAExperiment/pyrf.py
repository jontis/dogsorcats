from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
import pylab, numpy

class PyRF:
	def __init__(self, x, y, testx, testy):
		self.clf = RandomForestClassifier(n_estimators=len(set(y)), n_jobs=-1)
		self.clf = self.clf.fit(x, y)
		self.predy = self.clf.predict(testx)
		self.cm = confusion_matrix(self.predy, testy)
		self.true_positives_ratio = sum(self.cm.diagonal())/float(sum(sum(self.cm)))
		print 'true positives ', self.true_positives_ratio
		real_sum = numpy.sum(self.cm, 1)
		pred_sum = numpy.sum(self.cm, 0)
		correct_sum = self.cm.diagonal()
		correct_sum - real_sum
		self.hamming_loss = (1-self.true_positives_ratio) / float(max(y)+1)
		print 'hamming loss ', self.hamming_loss
		# create cm for show (zero 0,0 because it is to big)
		self.missmatrix = self.cm
		for i in range(self.cm.shape[0]):
			self.missmatrix[i,i] = 0
		pylab.matshow(self.missmatrix)
		pylab.title('Confusion Miss matrix')
		pylab.colorbar()
		pylab.ylabel('True label')
		pylab.xlabel('Predicted label')
		pylab.show()

