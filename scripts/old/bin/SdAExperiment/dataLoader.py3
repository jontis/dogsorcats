import pandas, random, gzip, pickle, numpy
from sklearn.preprocessing import scale

all_data = pandas.read_csv('/home/jonathan/kaggle/belkin/generatedData/multiTrainSetH1.csv')
all_data = all_data.drop(['Unnamed: 0'], axis=1)


# preprocess, scenter and scale
all_data_labels = all_data.pop('class').values
all_data = pandas.DataFrame(scale(all_data), index=all_data.index, columns=all_data.columns)
all_data['class'] = all_data_labels




# split in train, valid and test sets
# split 60 / 20 / 20?
n_samples = len(all_data.index)
train_index = random.sample(list(all_data.index), round(.6*n_samples))
train_data = all_data.loc[train_index]
all_data = all_data.drop(train_index)
train_labels = train_data.pop('class').values
train_data = train_data.values
print('train set ', train_data.shape) 

n_samples = len(all_data.index)
valid_index = random.sample(list(all_data.index), round(.5*n_samples))
valid_data = all_data.loc[valid_index]
all_data = all_data.drop(valid_index)
valid_labels = valid_data.pop('class').values
valid_data = valid_data.values
print('valid set ', valid_data.shape) 

test_data = all_data
test_labels = test_data.pop('class').values
test_data = test_data.values
all_data = None
print('test set ', test_data.shape) 

# collect the dataset and pickle
datasets = []
datasets.append((train_data, train_labels))
del train_data, train_labels
datasets.append((valid_data, valid_labels))
del valid_data, valid_labels
datasets.append((test_data, test_labels))
del test_data, test_labels

f = gzip.open('multiTrainSetH1.pkl.gz', 'wb')
pickle.dump(datasets, f) 
f.close()
