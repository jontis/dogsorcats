#buildTrainSet
require(stringr)
require(R.matlab)

CheckDataFilesAndParse <- function(house) {
  # check for presence of the files, reparse if there are none
  CheckForFiles <- function() {
    data.path <- '~/kaggle/belkin/generatedData'
    data.files <- list.files(path=data.path, pattern='*.Rdata')
    select.files.str <- paste('parsedTestSetH', house, sep='')
    data.files <- data.files[str_detect(data.files, select.files.str)] # only pick training files
    data.files
  }
    
  if (length(CheckForFiles()) < 1) {
    message('found no files, parsing')
    data.path <-  paste('~/kaggleSlow/belkin/sourceData/H', house, '/', sep='')
    data.files <- list.files(path=data.path, pattern='*.mat')
    data.files <- data.files[str_detect(data.files, 'Test')] # only pick test files
    
    for (i in 1:length(data.files)) {
      message('loading ', data.files[i])
      df.full.set <- JoinInfoChannels(ParseMatData(paste(data.path, data.files[i], sep='')))
      file.name <- paste('~/kaggle/belkin/generatedData/parsedTestSetH', house, '-', i, '.Rdata', sep='')
      save(df.full.set, file=file.name)
      rm(df.full.set)
      gc()
    }
  }else{
    message('found at least one file, not parsing')
  }
  CheckForFiles()
}

ParseMatData <- function(path) {
  temp.mat <- readMat(path)
  out <- list()
  out$HF <- t(temp.mat[[1]][[1]][[1]])
  out$ticksHF <- temp.mat[[1]][[2]][[1]]
  out$lfv1 <- temp.mat[[1]][[3]][[1]]
  out$lfi1 <- temp.mat[[1]][[4]][[1]]
  out$ticks1 <- temp.mat[[1]][[5]][[1]]
  out$lfv2 <- temp.mat[[1]][[6]][[1]]
  out$lfi2 <- temp.mat[[1]][[7]][[1]]
  out$ticks2 <- temp.mat[[1]][[8]][[1]]
  out
}

JoinInfoChannels <- function(data) {
  # PROCESSRAWBELKINDATA  Takes raw Current and Voltage harmonics to yield
  # Real, Reactive and Apparent Power along with Power Factor for each phase  
  
  # Compute Power
  # Calculate the real and imaginary power at each harmonic content
  l1p.all = data$lfv1 * Conj(data$lfi1)
  l2p.all = data$lfv2 * Conj(data$lfi2)
  
  # Compute net Complex power
  l1p.sum = rowSums(l1p.all)
  l2p.sum = rowSums(l2p.all)
  
  # Real, Reactive, Apparent powers, modulus
  l1re.all = Re(l1p.all)
  l1im.all = Im(l1p.all)
  l1app.all = abs(l1p.all)
  l1arg.all = Arg(l1p.all)
  l1pf.all = cos(l1arg.all)
  l1re.sum = Re(l1p.sum)
  l1im.sum = Im(l1p.sum)
  l1app.sum = abs(l1p.sum)
  l1arg.sum = Arg(l1p.sum)
  l1pf.sum = cos(l1arg.sum)
  l2re.all = Re(l2p.all)
  l2im.all = Im(l2p.all)
  l2app.all = abs(l2p.all)
  l2arg.all = Arg(l2p.all)
  l2pf.all = cos(l2arg.all)
  l2re.sum = Re(l2p.sum)
  l2im.sum = Im(l2p.sum)
  l2app.sum = abs(l2p.sum)
  l2arg.sum = Arg(l2p.sum)
  l2pf.sum = cos(l2arg.sum)  
  
  # arrange and send back, align with time tick
  ticks1 <- floor(data$ticks1) # the time ticks are messy, round off
  df.l1 <- data.frame(ticks=ticks1, l1re.all=l1re.all, l1im.all=l1im.all,
                      l1app.all=l1app.all, l1arg.all=l1arg.all, l1pf.all=l1pf.all,
                      l1re.sum, l1im.sum, l1app.sum, l1arg.sum, l1pf.sum)
  df.l1 <- df.l1[!duplicated(df.l1$ticks),] # remove duplicated timeticks (loss of data but probably acceptable)
  
  ticks2 <- floor(data$ticks2) # the time ticks are messy, round off
  df.l2 <- data.frame(ticks=ticks2, l2re.all=l2re.all, l2im.all=l2im.all,
                      l2app.all=l2app.all, l2arg.all=l2arg.all, l2pf.all=l2pf.all,
                      l2re.sum, l2im.sum, l2app.sum, l2arg.sum, l2pf.sum)
  df.l2 <- df.l2[!duplicated(df.l2$ticks),] # remove duplicated timeticks (loss of data but probably acceptable)
  
  ticksHF <- floor(data$ticksHF) # the time ticks are messy, round off
  df.HF <- data.frame(ticks=ticksHF, HF=data$HF)
  df.HF <- df.HF[!duplicated(df.HF$ticks),] # remove duplicated timeticks (loss of data but probably acceptable)
  
  # merge all
  df.l1l2 <- merge(df.l1, df.l2, by='ticks')
  out <- merge(df.l1l2, df.HF, by='ticks')
  out
}

BuildTestSet <- function(house) {
  message('start house ', house)
  
  data.files <- CheckDataFilesAndParse(house)
  data.path <- '~/kaggle/belkin/generatedData/'
  
  for (i in 1:length(data.files)) {
    load(paste(data.path, data.files[i], sep=''))
    
  
    multi.train.set <- rbind(positives, negatives)
    #plot(unlist(multi.train.set$ticks)) # plot kills running in headless process (screen)
    message('dupes ',sum(duplicated(positives$ticks)))
    rm()
    gc()
  }
  file.name <- paste(data.path, 'multiTestSetH', house, '.Rdata', sep="")
  save(list='multi.train.set', file=file.name)
  file.name <- paste(data.path, 'multiTestSetH', house, '.csv', sep="")
  write.csv(multi.train.set, file=file.name)
  message('done house ', house)
}

BuildTrainSetMultiClass(1)
BuildTrainSetMultiClass(2)
BuildTrainSetMultiClass(3)
BuildTrainSetMultiClass(4)