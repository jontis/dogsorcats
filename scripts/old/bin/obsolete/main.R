#!/usr/bin/Rscript
# preprocess?
# cap data, remove useless data
# TODO: create a training curve of train and cv error as a function of training set size
# TODO: dream up and evaluate more environmentals
# TODO: make a function that pulls and stores a sample of worst and best predictions for examination
# TODO: make a training case "jiggler" to bootstrat resample the training data
# TODO: make a second pass that multiplies and "jiggles" the misidentified cases
# TODO: denoise?
# TODO: check the auc

rm(list=ls(all=TRUE)[!(ls(all=TRUE) %in% '.rpud')]) # clear data, preserve .rpud or subsequent runs of rpud will fail
gc()                                                # force garbage collection since memory was cleared
options("max.print" = 150)                          # limit output to console
suppressWarnings(sink())                            # restore output to console if output has been disabled

setwd("/home/jonathan/kaggle/belkin/scripts")        # set working directory

require(stringr)                              # load 'stringr' for simpler string handling
#require(methods)                             # load methods for OO programming
require(multicore)
require(doMC)
require(plyr)                                 # better apply and large scale data handling
require(caret)                                # downSample

# configurations ---------------------------------------------
source('lib/configs.R')

if(config$general$cores > 1){registerDoMC(cores=config$general$cores) }else{registerDoSEQ() }  # initiate multicore

# override, run with these special options
config$general$dev.run <- FALSE                 # quick test run
config$general$dev.nrows <- 2000
config$general$regen.source.data <- TRUE
config$general$regen.training.set <- TRUE
config$general$enabled.ais <- list('rf')        # chose active AIs: svm, gbm, rf, nnet, glm
config$general$verbose <- FALSE                 # control chattiness
config$general$kfolds <- 3                      # folds for cross-validation, 2/3 gives fairly the same as 10
config$use.houses <- c(1)                       # 1 ....4

config$preprocess <- list()                     # options for preprocessing

config$train.set$type <- 'bucketized.cause'
config$train.set$balanced.downsample <- TRUE
config$train.set$maxrows <- 5000
config$train.set$remove.bad.feats <- FALSE
config$train.set$remove.nearzerovar <- FALSE
config$train.set$pca$enabled <- FALSE            # Principal Component Analysis
config$train.set$pca$kvectors <- 15             # PCA vectors to keep
config$train.set$preprocess <- FALSE
config$train.set$n.competing.examples <- 20


config$svm <- list()                            # options for svm
config$svm$use.gpu <- FALSE
config$svm$cost <- 5
config$svm$kernel.width <- 20
config$svm$error.type <- 'auc'

config$nnet <- list()                           # options for gbm
config$nnet$size <- 6                           # hidden layers
config$nnet$decay <- 1
config$nnet$trace <- TRUE                      # chatty
config$nnet$error.type <- 'auc'


config$environmentals <- list()
config$environmentals$enabled <- FALSE
config$environmentals$enabled.list <- list('total', 'nonzero', 'meannonzero', 'numberover10', 'numberover20')  # enabled generated causes


# includes ------------------------------------------------------
source('lib/dataLoaders.R')
source('lib/trainingSetPreparation.R')
source('lib/dataWriters.R')
#source('lib/ais.R')
source('lib/aisClassification.R') # testrun the classification ais and se if they perform better than regression


# dev_run -> change options for a quick run through
if(config$general$dev.run){
  message('doing a dev run')
  config$rf$ntree  <- 10                       # trees to build
  config$gbm$n.trees  <- 10                    # trees to build
  config$train.set$dev.run <- TRUE
}

# pull some examples from every appliance to check for misclassifications
multi.train.set.list <- CreateMultiClassSet(max.positives=100, max.negatives=100)
                      

# build house / appliance grid
loopstarttime=Sys.time()  # initiate progress indicator
writeProgress(new=TRUE)  # write progress: % done, time, expected time remaining, expected full time, written as a file because the multithreaded loop can't print
use.houses <- c(1) # 1 ... 4
store <- list() #storage variable for all data
for (house in config$use.houses) {
  store[[house]] <- list()
  use.appliances <- GetAvailableDataFileIDs(house)# get available appliance ids
  loop_count <- list(max=length(use.appliances))
  loop_count$now <- 0
  for (appliance in use.appliances) {
    store[[house]][[appliance]] <- list()
    
    
    # load and prepare source data --------------------------------------
    train.set <- GetDataFile(house, appliance)    
    
    # downsample if the data set is too large for memory, remove from majority class until set is balanced
    if (config$train.set$balanced.downsample) {
      message('balanced downsample')
      train.set$data <- downSample(train.set$data, train.set$class, list=FALSE, yname='redundant')
      train.set$data <- train.set$data[, !(names(train.set$data) %in% 'redundant')] # drop redundant col added by downsample
      train.set$class <- train.set$data[1]
      train.set$nrows <- nrow(train.set$data)
    }
    
    # fetch some samples for testing
    
    # make small test set for development
    if (config$general$dev.run & (train.set$nrow > config$general$dev.nrows)){
      train.set <- TrainSetShrink(train.set, nrows=config$general$dev.nrows)
      gc()
    }
    
    
    if (train.set$nrow > config$train.set$maxrows) {
      message('downsampled to config train.set maxrows')
      sample.vector <- sample(train.set$nrows, size=config$train.set$maxrows)
      train.set$data <- train.set$data[sample.vector,]
      train.set$class <- train.set$data[1]
      train.set$nrows <- nrow(train.set$data)
    }
    
    # need to weave in lots of positive examples from other appliances to make it disregard those
    train.set <- AppendOtherAppliances(train.set, multi.test.set.list, house, appliance)
    
    
    # remove columns with low prediction value (exchange for zero columns?)
    if (config$train.set$remove.bad.feats) {
      message('removing bad feats')
      end <- ncol(train.set$data)
      if (config$train.set$remove.nearzerovar) {
        train.set$kept.columns <- nearZeroVar(train.set$data[, 2:end]) # not col 1, it contains labels
        train.set$kept.columns <- !((2:end) %in% train.set$kept.columns) # turn coumn indices into bools
        train.set$kept.columns <- append((0<1), train.set$kept.columns) # add true for col 1
        train.set$data <- train.set$data[, train.set$kept.columns]        
      }else{
        train.set$kept.columns <- (0<colSums(train.set$data[, 2:end])) # not col 1, it contains labels
        train.set$kept.columns <- append((0<1), train.set$kept.columns) # add true for col 1
        train.set$data <- train.set$data[, train.set$kept.columns]        
      } 
    }
    
    multi.test.set <- list()
    multi.test.set$data <- multi.test.set.list[[house]]
    
    # preprocess, center and scale
    if (config$train.set$preprocess) {
      message('preprocessing')
      pre.processor <- preProcess(train.set$data[!(names(train.set$data) %in% 'class')])
      temp <- predict(pre.processor, train.set$data[!(names(train.set$data) %in% 'class')])
      train.set$data <- cbind(train.set$data[(names(train.set$data) %in% 'class')], temp)
      
      temp <- predict(pre.processor, multi.test.set$data[!(names(multi.test.set$data) %in% 'class')])
      multi.test.set$data <- cbind(multi.test.set$data[(names(multi.test.set$data) %in% 'class')], temp)
      rm(temp)
    }    
    
    
    if (config$train.set$pca$enabled) {
      message('pca')
      end <- ncol(train.set$data)
      pca.preprocess <- prcomp(train.set$data[, 2:end], scale = TRUE) # PCA, not col 1,2 for they don't contain feats
      screeplot(pca.preprocess, type="lines")
      #    sum((pca.preprocess$sdev[1:25])^2) / sum((pca.preprocess$sdev)^2) # 50 first components retain 95% variance
      file.name <- paste(config$general$generated.data, 'temp_train_set_data.Rdata', sep="")
      save(train.set, file=file.name) # cycle out the full train.set
      train.set$data <- cbind(train.set$data[1], as.data.frame(pca.preprocess$x[, 1:config$train.set$pca$kvectors]))
      
      multi.test.set$data <- cbind(multi.test.set$data[1], as.data.frame(predict(pca.preprocess, multi.test.set$data[, 2:end])[, 1:config$train.set$pca$kvectors]))
    
    }
    
    

    
    # generate extra training data and train AIs --------------------------------------------------------
        
    store[[house]][[appliance]]$multi.test.class <- multi.test.set$data[1]
    # train the ais and generate the model, store performance descriptors,
    for(ai.i in config$general$enabled.ais){
      store[[house]][[appliance]][[ai.i]] <- list()
      gc()
      loop_count$now = loop_count$now + 1
      message() # this message is here to generate a linefeed for the messages in the end of this loop
      message(format(Sys.time(), "%H:%M:%S"), ', starting modelling ai: ', ai.i)
      
      # try changing parameters for training, run naive at first
      
      mini.ai.timer <- system.time(
        #errors[[ai.i]] <- AiOne(config, ai.i, train.set$data[c(-1,-2)], train.set$data[1], actions=c('error')) # train ai
        #store[[house]][[appliance]][[ai.i]]$cvpred <- AiOne(config, ai.i, train.set$data[-1], train.set$data[1], actions=c('cvpred'))$cvpred # train ai
        #models[[ai.i]] <- AiOne(config, ai.i, train.set$data[c(-1,-2)], train.set$data[1], actions=c('model')) # train ai
        store[[house]][[appliance]][[ai.i]]$prediction <- AiOne(config, ai.i, train.set$data[-1], train.set$data[1], newcause=multi.test.set$data[-1], actions=c('predict'))$predict 
        
      )
      
      message(format(Sys.time(), "%H:%M:%S"), ', ai time: ',round(mini.ai.timer[3], 4), '; ', appendLF=FALSE)
      
      
      # write data -----------------------------------------------------------------------------
      filename.full.string <- ifelse(length(config$general$enabled.ais)>1, 'many', config$general$enabled.ais)
      filename <- paste(config$general$generated_data_dir, 'store_', filename.full.string, '.Rdata', sep="")
      save(list = "store", file=filename)
      filename <- paste(config$general$generated_data_dir, 'config_', filename.full.string, '.Rdata', sep="")
      save(list = "config", file=filename)  
      share_done = loop_count$now / loop_count$max 
      writeProgress(share_done, loopstarttime)
      messageProgress(share_done, loopstarttime)
      
    }
    
    # swap out the training set
    rm(train.set)
    gc()
    
    #source(mainTest.R) # call test set
  }
}

message('all finished')