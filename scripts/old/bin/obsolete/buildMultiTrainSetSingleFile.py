#buildTrainSet
import glob, pandas, numpy, gzip, pickle
from progressbar import ProgressBar, Percentage, Bar, RotatingMarker, ETA
from scipy import io
from cmath import phase
from math import *
from sklearn.preprocessing import StandardScaler

def CheckDataFilesAndParse(house, set_type='train'):
	house = str(house)
	# check for presence of the files, reparse if there are none
	data_path_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	data_path_source = '/home/jonathan/kaggleSlow/belkin/sourceData/'
	
	def ParseMatData(path):
		# Load the .mat files
		testData = io.loadmat(path)
		
		# Extract tables
		buf = testData['Buffer']
		v1 = buf['LF1V'][0][0]
		i1 = buf['LF1I'][0][0]
		v2 = buf['LF2V'][0][0]
		i2 = buf['LF2I'][0][0]
		t1 = buf['TimeTicks1'][0][0]
		t2 = buf['TimeTicks2'][0][0]
		hf = buf['HF'][0][0]
		thf = buf['TimeTicksHF'][0][0]
		out_lf = {'v1': v1, 'i1': i1, 'v2': v2, 'i2': i2, 't1': t1, 't2': t2}
		out_hf = {'hf': hf, 'thf': thf}
		return out_lf, out_hf
		
	
	def CombinePower(data, smooth=False):
		# PROCESSRAWBELKINDATA  Takes raw Current and Voltage harmonics to yield
		# Real, Reactive and Apparent Power along with Power Factor for each phase  
		t1 = data['t1'].flatten()
		t2 = data['t2'].flatten()
		# Calculate power (by convolution)
		p1 = data['v1'] * data['i1'].conjugate()
		p2 = data['v2'] * data['i2'].conjugate()
		#
		if smooth: # quick and dirty smooth with 5 points mean window
			p1 = (p1[:-4, :] + p1[1:-3, :] + p1[2:-2, :] + p1[3:-1, :] + p1[4:, :]) / 5
			p2 = (p2[:-4, :] + p2[1:-3, :] + p2[2:-2, :] + p2[3:-1, :] + p2[4:, :]) / 5
			t1 = t1[2:-2]
			t2 = t2[2:-2]
		
		ComplexPower1 = p1.sum(axis=1)
		ComplexPower2 = p2.sum(axis=1)
		# Extract components
		p1_re = ComplexPower1.real
		p1_im = ComplexPower1.imag
		p1_app = abs(ComplexPower1)
		p2_re = ComplexPower2.real
		p2_im = ComplexPower2.imag
		p2_app = abs(ComplexPower2)
		#
		l1_pf = pandas.np.asarray([cos(phase(p1[i,0])) for i in range(len(p1[:,0]))])
		l2_pf = pandas.np.asarray([cos(phase(p2[i,0])) for i in range(len(p2[:,0]))])
		#
		#p12_app = p1_app + p2_app
		
		out = [pandas.DataFrame({'t': t1, 'p1_re': p1_re, 'p1_im': p1_im, 
				'p1_app': p1_app, 'l1_pf': l1_pf}),
				pandas.DataFrame({'t': t2, 'p2_re': p2_re, 'p2_im': p2_im,
				 'p2_app': p2_app, 'l2_pf': l2_pf})]
		return out
	
	
	def MergeLfToDataFrame(li_df_lf):
		li_df_lf[0]['t'] = numpy.round(li_df_lf[0]['t']) # round off to full seconds
		li_df_lf[1]['t'] = numpy.round(li_df_lf[1]['t']) # round off to full seconds
		df_lf0 = li_df_lf[0].drop_duplicates(cols='t')
		df_lf1 = li_df_lf[1].drop_duplicates(cols='t')
		return pandas.merge(df_lf0, df_lf1, on='t')
	
	
	def MergeHfToDataFrame(dict_hf, smooth=False):
		hf = dict_hf['hf'].transpose()
		thf = numpy.round(dict_hf['thf']).flatten()
		if smooth:
			hf = (hf[:-4, :] + hf[1:-3, :] + hf[2:-2, :] + hf[3:-1, :] + hf[4:, :])/5
			thf = thf[2:-2]
		
		df_hf = pandas.DataFrame(hf)
		df_hf['t'] = thf
		df_hf['month'] = pandas.to_datetime(thf[0], unit='s').month
		df_hf['wday'] = pandas.to_datetime(thf[0], unit='s').dayofweek
		df_hf['hour'] = pandas.to_datetime(thf, unit='s').hour
		df_hf = df_hf.drop_duplicates(cols='t')
		return df_hf
	
	
	def CheckForParsedFiles(set_type='train'):
		return sorted(glob.glob(data_path_gen + 'parsed_' + set_type + '_H' + house + '-?.h5'))
	
	
	def GetSourceDataFileNames(set_type='train'):
		if set_type=='train':
			return sorted(glob.glob(data_path_source + 'H' + house + '/'+ 'Tagged*.mat')) # only pick training files
		else:
			return sorted(glob.glob(data_path_source + 'H' + house + '/'+ 'Testing*.mat')) # only pick test files
	
	
	if (len(CheckForParsedFiles(set_type)) < 1):
		print 'found no files, parsing'
		data_files = GetSourceDataFileNames(set_type)
		for i in range(len(data_files)):
			print 'loading ' + data_files[i]
			dict_lf, dict_hf = ParseMatData(data_files[i])
			li_df_lf = CombinePower(dict_lf, smooth=True)
			df_lf = MergeLfToDataFrame(li_df_lf)
			df_hf = MergeHfToDataFrame(dict_hf, smooth=True)
			file_name = 'parsed_' + set_type + '_H' + house + '-' + str(i) + '.h5'
			df_hf.to_hdf(data_path_gen + file_name, 'hf')
			df_lf.to_hdf(data_path_gen + file_name, 'lf')
			del df_hf, df_lf
	else:
		print 'found at least one file, not parsing'
	return CheckForParsedFiles(set_type)


def CombinePreparsedFiles(house, set_type='train'):
	data_path_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	data_files = CheckDataFilesAndParse(house, set_type)
	df_hflf = pandas.DataFrame()
	for i in range(len(data_files)):
		print 'loading ' + data_files[i]
		df_hf = pandas.read_hdf(data_files[i], 'hf')
		df_lf = pandas.read_hdf(data_files[i], 'lf')
		df_hflf = df_hflf.append(pandas.merge(df_hf, df_lf, on='t'))
	df_hflf = df_hflf.reset_index() # reset index to work after append
	file_name = 'parsed_' + set_type + '_H' + house + '-raw.h5'
	df_hflf.to_hdf(data_path_gen + file_name, 'df')
	return df_hflf


def ParseLabels(house):
	CombinePreparsedFiles(house)
	print 'start parsing labels house ' + house
	data_path_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	data_path_source = '/home/jonathan/kaggleSlow/belkin/sourceData/'
	data_file = 'H' + house + '/AllTaggingInfo.csv'
	label_data = pandas.read_csv(data_path_source + data_file, header=None)
	appliance_label_key = label_data.drop_duplicates(cols=0).iloc[:,0:2]
	appliance_label_key = appliance_label_key.sort(columns=0)
	file_name = 'parsed_train_H' + house + '-raw.h5'
	df_hflf = pandas.read_hdf(data_path_gen + file_name, 'df')
	ticks = df_hflf['t'].values
	del df_hflf
	appliance_ids = [0] + appliance_label_key[0].tolist()
	label_mask = numpy.zeros((len(ticks), len(appliance_ids)),  dtype=numpy.bool)
	for i in range(label_data.shape[0]):
		tick_mask = (ticks >= label_data.iloc[i,2]) & (ticks <= label_data.iloc[i,3])
		label_mask[tick_mask, label_data.iloc[i,0]] = 1
	df_label_mask = pandas.DataFrame(label_mask)
	df_label_mask['ticks'] = ticks
	data_file = 'labelsH' + house + '.h5'
	df_label_mask.to_hdf(data_path_gen + data_file, 'df')
	# pylab.imshow(df_label_mask.drop(['ticks'], axis=1).values, aspect='auto')
	# pylab.show()
	return df_label_mask


def DfRemoveArtefacts(df_data):
	while 'index' in df_data.keys(): df_data.pop('index') # remove artefacts
	while 'level_0' in df_data.keys(): df_data.pop('level_0') # remove artefacts
	return df_data


def BuildMultiTrainSet(house):
	data_path_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	save_file = 'multi_train_H' + house + '-raw.h5'
	# check for file and bypass processing if there is a cached version
	if len(glob.glob(data_path_gen + save_file)) > 0:
		print 'found premade train set, skipping build'
		df_multi_train = pandas.read_hdf(data_path_gen + save_file, 'df')
		return df_multi_train
	
	ParseLabels(house)
	print 'start building multi train set house ' + house
	data_file = 'parsed_train_H' + house + '-raw.h5'
	label_file = 'labelsH' + house + '.h5'
	df_labels = pandas.read_hdf(data_path_gen + label_file, 'df')
	df_ticks = df_labels.pop('ticks')
	
	max_positives = 500
	max_negatives = 500
	n_label_ids = df_labels.shape[1]
	df_positives = pandas.DataFrame()
	df_pos_index = pandas.DataFrame()
	
	# progressbar
	widgets = ['Labels: ', Percentage(), ' ', Bar(marker=RotatingMarker()), ' ', ETA()]
	pbar = ProgressBar(widgets=widgets, maxval=n_label_ids).start()
	
	df_data = pandas.read_hdf(data_path_gen + data_file, 'df')
	for i in range(1, n_label_ids):         # skip column 0, this is the negative index...
		if sum(df_labels.values[:,i]) > 0: # miminum positive examples to try this method
			# get the data, expand here for more features
			pbar.update(i)
			pos_index = numpy.arange(df_labels.shape[0])[(df_labels.iloc[:, i] == 1)]
			pos_index = numpy.random.choice(pos_index, max_positives)] # downsample / upsample to desired amount
			df_index_label = df_pos_index.append(
				pandas.DataFrame({'data_index': pos_index, 'label': i})
	pbar.finish()

	neg_index = numpy.arange(df_labels.shape[0])[(numpy.sum(df_labels.values, axis=1)==0)]
	neg_index = numpy.random.choice(neg_index, max_negatives)] # downsample / upsample to desired amount
	df_index_label = df_pos_index.append(
		pandas.DataFrame({'data_index': neg_index, 'label': 0})

	df_multi_train = df_data.iloc[df_index_label['data_index'], :]
	del df_data # cleanup
	df_multi_train['label'] = df_index_label['label']
	#df_multi_train = df_multi_train.reset_index() # reset index after appends
	df_multi_train = DfRemoveArtefacts(df_multi_train)
	df_multi_train = df_multi_train.astype('float32') # downcast for size and CUDA
	df_multi_train.to_hdf(data_path_gen + save_file, 'df')
	print 'done house ' + house
	return df_multi_train


def BuildTestSet(house):
	data_path_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	save_file = 'test_H' + house + '-raw.h5'
    # check for file and bypass processing if there is a cached version
	if len(glob.glob(data_path_gen + save_file)) > 0:
		print 'found premade test set, skipping build'
		df_test = pandas.read_hdf(data_path_gen + save_file, 'df')
		return df_test
	print 'start building test set ' + house
	df_sub = pandas.read_csv(data_path_gen + 'SampleSubmission.csv')
	print 'doing house ' + house
	target_appl =  set(df_sub[df_sub['House'] == 'H' + house]['Appliance'])
	target_ticks = numpy.array(sorted([x for x in set(df_sub[df_sub['House'] == 'H' + house]['TimeStamp'])]))
	CombinePreparsedFiles(house, set_type='test')
	data_file = 'parsed_test_H' + house + '-raw.h5'
	df_data = pandas.read_hdf(data_path_gen + data_file, 'df')
	df_data['t'] = df_data['t'].astype(int) # int the t column before you make it an index or your selections with int will go bad
	df_data.set_index(df_data['t'], inplace=True)
	df_data = DfRemoveArtefacts(df_data)
	df_test = df_data.loc[target_ticks]
	new_target_ticks = target_ticks
	while sum(numpy.isnan(df_test.iloc[:,0])) > 0: # check for missing rows, meaning keys were missing in the index
		t_miss_loc = numpy.isnan(df_test.iloc[:,0])
		print 'adjusted missed target ticks ' + str(sum(t_miss_loc))
		new_target_ticks[t_miss_loc] = new_target_ticks[t_miss_loc]+1 # try to get the ticks after
		df_test = df_data.loc[new_target_ticks]
		df_test['t'] = target_ticks
	
	df_test.set_index(df_test['t'], inplace=True)
	df_test = DfRemoveArtefacts(df_test)
	df_test = df_test.astype('float32') # downcast for size and CUDA
	df_test.to_hdf(data_path_gen + save_file, 'df')
	print 'done house ' + house
	return df_test


def PreProcessData(house, set_type='train', df_data=None):
	house = str(house)
	data_path_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	if df_data is None: 
		if set_type=='train': 
			df_data = BuildMultiTrainSet(house)
		else:
			df_data = BuildTestSet(house)
	print 'preprocessing'
	# preprocess, scenter and scale
	p_file_name = 'preprocessorH' + house +'.pkl'
	df_ticks = df_data.pop('t') # don't preprocess ticks
	if set_type=='train': 
		df_labels = df_data.pop('label').values
		myscale = StandardScaler()
		df_data = pandas.DataFrame(myscale.fit_transform(df_data), index=df_data.index, columns=df_data.columns)
		df_data['label'] = df_labels
		preprocessor_file = open(data_path_gen + p_file_name, 'wb') # pickle the preprocessor to be identical when processing test data
		pickle.dump(myscale, preprocessor_file)
		preprocessor_file.close()
	else:
		preprocessor_file = open(data_path_gen + p_file_name, 'rb') # load the preprocessor
		myscale = pickle.load(preprocessor_file)
		preprocessor_file.close()
		df_data = pandas.DataFrame(myscale.transform(df_data), index=df_data.index, columns=df_data.columns)
	df_data = df_data.astype('float32') # downcast for size and CUDA
	df_data['t'] = df_ticks
	
	print df_data
	if set_type=='train':
		print 'writing to ' + data_path_gen + 'multi_train_H' + house + '-full.h5'
		df_data.to_hdf(data_path_gen + 'multi_train_H' + house + '-full.h5', 'df')
	else:
		print 'writing to ' + data_path_gen + 'refined_test_H' + house + '.h5'
		df_data.to_hdf(data_path_gen + 'refined_test_H' + house + '.h5', 'df')


PreProcessData(1, 'train')
PreProcessData(2, 'train')
PreProcessData(3, 'train')
PreProcessData(4, 'train')

PreProcessData(1, 'test')
PreProcessData(2, 'test')
PreProcessData(3, 'test')
PreProcessData(4, 'test')


"""
  # leftover code

  # Compute Power
  # Calculate the real and imaginary power at each harmonic content
  l1p.all = data$lfv1 * Conj(data$lfi1)
  l2p.all = data$lfv2 * Conj(data$lfi2)
   
  # Real, Reactive, Apparent powers, modul
  l1re.all = Re(l1p.all)
  l1im.all = Im(l1p.all)
  l1app.all = abs(l1p.all)
  l1arg.all = Arg(l1p.all)
  l1pf.all = cos(l1arg.all)
  l1re.sum = Re(l1p.sum)
  l1im.sum = Im(l1p.sum)
  l1app.sum = abs(l1p.sum)
  l1arg.sum = Arg(l1p.sum)
  l1pf.sum = cos(l1arg.sum)
  l2re.all = Re(l2p.all)
  l2im.all = Im(l2p.all)
  l2app.all = abs(l2p.all)
  l2arg.all = Arg(l2p.all)
  l2pf.all = cos(l2arg.all)
  l2re.sum = Re(l2p.sum)
  l2im.sum = Im(l2p.sum)
  l2app.sum = abs(l2p.sum)
  l2arg.sum = Arg(l2p.sum)
  l2pf.sum = cos(l2arg.sum)  
  
  # arrange and send back, align with time tick
  ticks1 <- floor(data$ticks1) # the time ticks are messy, round off
  df.l1 <- data.frame(ticks=ticks1, l1re.all=l1re.all, l1im.all=l1im.all,
                      l1app.all=l1app.all, l1arg.all=l1arg.all, l1pf.all=l1pf.all,
                      l1re.sum, l1im.sum, l1app.sum, l1arg.sum, l1pf.sum)
  df.l1 <- df.l1[!duplicated(df.l1$ticks),] # remove duplicated timeticks (loss of data but probably acceptable)
  
  ticks2 <- floor(data$ticks2) # the time ticks are messy, round off
  df.l2 <- data.frame(ticks=ticks2, l2re.all=l2re.all, l2im.all=l2im.all,
                      l2app.all=l2app.all, l2arg.all=l2arg.all, l2pf.all=l2pf.all,
                      l2re.sum, l2im.sum, l2app.sum, l2arg.sum, l2pf.sum)
  df.l2 <- df.l2[!duplicated(df.l2$ticks),] # remove duplicated timeticks (loss of data but probably acceptable)
  
  ticksHF <- floor(data$ticksHF) # the time ticks are messy, round off
  df.HF <- data.frame(ticks=ticksHF, HF=data$HF)
  df.HF <- df.HF[!duplicated(df.HF$ticks),] # remove duplicated timeticks (loss of data but probably acceptable)
  
  # merge all
  df.l1l2 <- merge(df.l1, df.l2, by='ticks')
  out <- merge(df.l1l2, df.HF, by='ticks')
  out
}
"""
