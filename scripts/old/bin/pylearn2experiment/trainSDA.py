from pylearn2.config import yaml_parse
import belkinDatasetCreatorH5 as belkindata

house=3
train_data_sample = belkindata.BelkinDataset(house=house, set_type='tt', set_which='test', y_type='normal')
n_inputs = train_data_sample.X.shape[1]
del train_data_sample

layer1_yaml = open('dae_l1.yaml', 'r').read()
hyper_params_l1 = {'batch_size' : 100,
                   'monitoring_batches' : 5,
                   'house' : house,
                   'nvis' : n_inputs,
                   'nhid' : 1000,
                   'max_epochs' : 10,
                   'save_path' : '.'}
layer1_yaml = layer1_yaml % (hyper_params_l1)

train = yaml_parse.load(layer1_yaml)
train.main_loop()

layer2_yaml = open('dae_l2.yaml', 'r').read()
hyper_params_l2 = {'batch_size' : 100,
                   'monitoring_batches' : 5,
                   'house' : house,
                   'nvis' : hyper_params_l1['nhid'],
                   'nhid' : 500,
                   'max_epochs' : 10,
                   'save_path' : '.'}
layer2_yaml = layer2_yaml % (hyper_params_l2)

train = yaml_parse.load(layer2_yaml)
train.main_loop()

mlp_yaml = open('dae_mlp.yaml', 'r').read()
hyper_params_mlp = {'batch_size' : 100,
                    'house' : house,
                    'nvis' : n_inputs,
                    'max_epochs' : 10,
                    'save_path' : '.'}
mlp_yaml = mlp_yaml % (hyper_params_mlp)

train = yaml_parse.load(mlp_yaml)
train.main_loop()
