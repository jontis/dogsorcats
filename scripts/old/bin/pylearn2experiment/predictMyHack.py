import sys, os, pylab
from pylearn2.utils import serial
from pylearn2.config import yaml_parse
import belkinDatasetCreatorH5


#hardcode
model = serial.load('dae_mlp.pkl' )
test_data = belkinDatasetCreatorH5.BelkinDataset(house=3, set_type='submission', drop_feats=None)

# use smallish batches to avoid running out of memory
batch_size = 100
model.set_batch_size(batch_size)

# dataset must be multiple of batch size of some batches will have
# different sizes. theano convolution requires a hard-coded batch size
m = test_data.X.shape[0]
extra = batch_size - m % batch_size
assert (m + extra) % batch_size == 0
import numpy as np
if extra > 0:
	test_data.X = np.concatenate((test_data.X, np.zeros((extra, test_data.X.shape[1]),
	dtype=test_data.X.dtype)), axis=0)
assert test_data.X.shape[0] % batch_size == 0


X = model.get_input_space().make_batch_theano()
Y = model.fprop(X)

from theano import tensor as T

y = T.argmax(Y, axis=1)

from theano import function

f = function([X], y)


y = []

for i in xrange(test_data.X.shape[0] / batch_size):
	print str(i)
	x_arg = test_data.X[i*batch_size:(i+1)*batch_size,:]
	if X.ndim > 2:
		x_arg = test_data.get_topological_view(x_arg)
	y.append(f(x_arg.astype(X.dtype)))

y = np.concatenate(y)
assert y.ndim == 1
assert y.shape[0] == test_data.X.shape[0]
# discard any zero-padding that was used to give the batches uniform size
y = y[:m]

bool_image = np.zeros(( y.shape[0], max(y)+1))
for i in xrange(max(y)):
	bool_image[:, i] = (y == i)

pylab.imshow(bool_image, aspect='auto')
pylab.show()


#def PredictSubmission(self, house, drop_feats=None):
	## load model
	#data_path = '/home/jonathan/kaggle/belkin/generatedData/'
	#m_file_name = 'model_H' + str(house) +'.pkl'	
	#model_file = open(data_path + m_file_name, 'rb')
	#clf = pickle.load(model_file)
	#model_file.close()
	
	## load test data
	#test_data = belkinDatasetCreatorH5.BelkinDataset(house=house, set_type='submission', drop_feats=drop_feats)
	#prob_matrix = clf.predict_proba(test_data.X)
	#label = clf.predict(test_data.X)
	#pylab.imshow(prob_matrix, aspect='auto')
	#pylab.show()
