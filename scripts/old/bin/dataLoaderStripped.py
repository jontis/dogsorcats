import pandas, random, gzip, pickle, numpy
from sklearn.preprocessing import StandardScaler

def DataLoaderStripped(house, set_type='train'):
	house = str(house)
	data_path = '/home/jonathan/kaggle/belkin/generatedData/'
	if set_type=='train':
		file_name = 'multiTrainSetH' + house
	else:
		file_name = 'refinedTestH' + house
		
	print 'processing ' + file_name
		
	all_data = pandas.read_csv(data_path + file_name + '.csv')
	all_data = all_data.drop(['Unnamed: 0'], axis=1)
	
	# preprocess, scenter and scale
	p_file_name = 'preprocessorH' + house +'.pkl'
	all_data_ticks = all_data.pop('ticks') # don't preprocess ticks
	if set_type=='train': 
		all_data_labels = all_data.pop('class').values
		myscale = StandardScaler()
		all_data = pandas.DataFrame(myscale.fit_transform(all_data), index=all_data.index, columns=all_data.columns)
		all_data['class'] = all_data_labels
		preprocessor_file = open(data_path + p_file_name, 'wb') # pickle the preprocessor to be identical when processing test data
		pickle.dump(myscale, preprocessor_file)
		preprocessor_file.close()
	else:
		preprocessor_file = open(data_path + p_file_name, 'rb') # load the preprocessor
		myscale = pickle.load(preprocessor_file)
		preprocessor_file.close()
		all_data = pandas.DataFrame(myscale.transform(all_data), index=all_data.index, columns=all_data.columns)
	all_data['ticks'] = all_data_ticks

	
	# persist to hdf, fast and small
	print all_data
	if set_type=='train':
		print 'writing to ' + data_path + file_name + '-full.h5'
		all_data.to_hdf(data_path + file_name + '-full.h5', 'full')
	else:
		print 'writing to ' + data_path + 'refinedTestH' + house + '.h5'
		all_data.to_hdf(data_path + 'refinedTestH' + house + '.h5', 'test')

DataLoaderStripped(1)
DataLoaderStripped(2)
DataLoaderStripped(3)
DataLoaderStripped(4)

DataLoaderStripped(1, set_type='test')
DataLoaderStripped(2, set_type='test')
DataLoaderStripped(3, set_type='test')
DataLoaderStripped(4, set_type='test')

