"""model creator
"""
import pandas, pickle, pylab, numpy
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import hamming_loss
import belkinDatasetCreatorH5
#config
drop_feats=None
raw=False

def CreateModel(house, measure_loss=False, drop_feats=drop_feats, raw=raw):
	house = str(house)
	# get some data
	if measure_loss:
		train_data = belkinDatasetCreatorH5.BelkinDataset(set_type='tt', set_which='train', house=house, drop_feats=drop_feats, raw=raw)
		X = train_data.X
		y = train_data.y
		test_data = belkinDatasetCreatorH5.BelkinDataset(set_type='tt', set_which='test', house=house, drop_feats=drop_feats, raw=raw)
		test_X = test_data.X
		test_y = test_data.y
	else:
		all_data = belkinDatasetCreatorH5.BelkinDataset(house=house, drop_feats=drop_feats)
		X = all_data.X
		y = all_data.y
	
	#build a classifier
	print 'building model house ' + house
	clf = RandomForestClassifier(n_estimators=100, n_jobs=-1, criterion="entropy", max_depth=None,
	                             bootstrap=False, min_samples_leaf=5, min_samples_split=10,  max_features='auto')
	print 'initiated model object'
	clf = clf.fit(X, y)
	print 'fitted model'
	data_path = '/home/jonathan/kaggle/belkin/generatedData/'
	m_file_name = 'model_H' + str(house) +'.pkl'
	model_file = open(data_path + m_file_name, 'wb') # pickle the model for later forensics
	pickle.dump(clf, model_file)
	model_file.close()
	
	if measure_loss:
		pred_test_y = clf.predict(test_X)
		kaggle_hamming = hamming_loss(test_y, pred_test_y) / len(set(test_y))
		print 'percent miss: ' + str(kaggle_hamming)
		prob_matrix = clf.predict_proba(test_X)
		pylab.imshow(prob_matrix, aspect='auto')
		pylab.show()
		return pred_test_y
	else:
		return clf


def Predict(model, test_X):
	predy = model.predict(test_X)


class Submission:
	def __init__(self):
		data_path = '/home/jonathan/kaggle/belkin/generatedData/'
		file_name = 'SampleSubmission.csv'
		df_subm = pandas.read_csv(data_path + file_name)
		houses = [1, 2, 3, 4]
		appliances = {}       # {house : appliances}
		ticks = {}            # {house : ticks}
		for h in houses:
			key = 'H' + str(h)
			appliances[h] = set(df_subm.loc[df_subm.loc[:, 'House']==key, 'Appliance'])
			ticks[h] = set(df_subm.loc[df_subm.loc[:, 'House']==key, 'TimeStamp'])
		self.df_subm = df_subm
	
	
	def PredictSubmission(self, house, drop_feats=drop_feats, bool_image=False, raw=raw):
		# load model
		data_path = '/home/jonathan/kaggle/belkin/generatedData/'
		m_file_name = 'model_H' + str(house) +'.pkl'	
		print 'loading ' + m_file_name
		model_file = open(data_path + m_file_name, 'rb')
		clf = pickle.load(model_file)
		model_file.close()
		print 'loading test data'
		
		# load test data
		test_data = belkinDatasetCreatorH5.BelkinDataset(house=house, set_type='submission', drop_feats=drop_feats, raw=raw)
		print 'done loading test data, now predicting'
		prob_matrix = clf.predict_proba(test_data.X)
		label = clf.predict(test_data.X)
		bool_matrix = prob_matrix > .5
		print 'visualizing'
		if bool_image:
			pylab.imshow(bool_matrix, aspect='auto')
		else:
			pylab.imshow(prob_matrix, aspect='auto')
		pylab.show()

if __name__ == '__main__':
	CreateModel(1, drop_feats=drop_feats, raw=raw)
	CreateModel(2, drop_feats=drop_feats, raw=raw)
	CreateModel(3, drop_feats=drop_feats, raw=raw)
	CreateModel(4, drop_feats=drop_feats, raw=raw)


# generate submission files
# format as: 
# Id,House,Appliance,TimeStamp,Predicted
# 1,H1,30,1334300400,0
# 2,H1,29,1334300400,0
# 3,H1,15,1334300400,0
