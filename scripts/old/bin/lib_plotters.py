import matplotlib.pyplot as plt

def myplot_dis(x, y):
    # scatterplot
    plt.scatter(x, y, s, c="g", alpha=0.5, marker=r'$\clubsuit$',
                label="Luck")
    plt.xlabel("Leprechauns")
    plt.ylabel("Gold")
    plt.legend(loc=2)
    plt.show()


def myplot(x, y):

    # red dashes, blue squares and green triangles
    plt.plot(x, y, 'g^')
    plt.show()

def myplot2(x, y, x2, y2):

    # red dashes, blue squares and green triangles
    plt.plot(x, y, 'g^', x2, y2, 'r--')
    plt.show()