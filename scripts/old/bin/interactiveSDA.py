import pickle
import gzip
import os
import sys
import time

import numpy

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from logistic_sgd import LogisticRegression, load_data
from mlp import HiddenLayer
from dA import dA
from stackedDenoisingAutoencoderPy3 import SdA

# post analysis and plotting
from sklearn.metrics import confusion_matrix
import pylab as pl


finetune_lr=1
pretraining_epochs=500
pretrain_lr=25
training_epochs=1000
dataset='multiTrainSetH1.pkl.gz'
batch_size=1
datasets = load_data(dataset)

train_set_x, train_set_y = datasets[0]
valid_set_x, valid_set_y = datasets[1]
test_set_x, test_set_y = datasets[2]
dim_input = train_set_x.get_value(borrow=True).shape[1]

# compute number of minibatches for training, validation and testing
n_train_batches = train_set_x.get_value(borrow=True).shape[0]
n_train_batches /= batch_size
n_train_batches = int(n_train_batches)  # bugfix, range wants this as int later

# numpy random generator
numpy_rng = numpy.random.RandomState(89677)
print('... building the model')
# construct the stacked denoising autoencoder class
sda = SdA(numpy_rng=numpy_rng, n_ins=dim_input,
		  hidden_layers_sizes=[1000],
		  n_outs=40)

#########################
# PRETRAINING THE MODEL #
#########################
print('... getting the pretraining functions')
pretraining_fns = sda.pretraining_functions(train_set_x=train_set_x,
											batch_size=batch_size)

print('... pre-training the model')
start_time = time.clock()
## Pre-train layer-wise
corruption_levels = [.1, .2, .3]
done_looping = False
epoch = 0
best_cost = 0
prepatience = 3
missed_improvement = 0
improvement_threshold = 0.02  # a relative improvement of this much is considered significant
for i in range(sda.n_layers):
	# go through pretraining epochs     
	while (epoch < pretraining_epochs) and (not done_looping):
		epoch +=1
		# go through the training set
		c = []
		for batch_index in range(n_train_batches):
			c.append(pretraining_fns[i](index=batch_index,
					 corruption=corruption_levels[i],
					 lr=pretrain_lr))
		this_cost = numpy.mean(c)
		
		# loop control
		if best_cost == 0: best_cost = this_cost
		if (this_cost < (best_cost - improvement_threshold * abs(best_cost))): 
			best_cost = this_cost
			missed_improvement = 0
		else:
			missed_improvement += 1
			if missed_improvement >= prepatience: done_looping = True
			 
		print('Pre-training layer %i, epoch %d, cost ' % (i, epoch), end=' ')
		print(numpy.mean(c))
		
		

end_time = time.clock()

########################
# FINETUNING THE MODEL #
########################

# get the training, validation and testing function for the model
print('... getting the finetuning functions')
train_fn, validate_model, test_model, predict_model = sda.build_finetune_functions(
			datasets=datasets, batch_size=batch_size,
			learning_rate=finetune_lr)

print('... finetunning the model')
# early-stopping parameters
patience = 10 * n_train_batches  # look as this many examples regardless
patience_increase = 2.  # wait this much longer when a new best is found
improvement_threshold = 0.995  # a relative improvement of this much is considered significant
validation_frequency = min(n_train_batches, patience / 2)
							  # go through this many
							  # minibatche before checking the network
							  # on the validation set; in this case we
							  # check every epoch

best_params = None
best_validation_loss = numpy.inf
test_score = 0.
start_time = time.clock()

done_looping = False
epoch = 0

while (epoch < training_epochs) and (not done_looping):
	epoch = epoch + 1
	for minibatch_index in range(n_train_batches):
		minibatch_avg_cost = train_fn(minibatch_index)
		iter = (epoch - 1) * n_train_batches + minibatch_index

		if (iter + 1) % validation_frequency == 0:
			validation_losses = validate_model()
			this_validation_loss = numpy.mean(validation_losses)
			prediction_out = [i.item() for i in predict_model()]
			print(('epoch %i, minibatch %i/%i, validation error %f %%' %
				  (epoch, minibatch_index + 1, n_train_batches,
				   this_validation_loss * 100.)))

			# if we got the best validation score until now
			if this_validation_loss < best_validation_loss:

				#improve patience if loss improvement is good enough
				if (this_validation_loss < best_validation_loss *
					improvement_threshold):
					patience = max(patience, iter * patience_increase)

				# save best validation score and iteration number
				best_validation_loss = this_validation_loss
				best_iter = iter

				# test it on the test set
				test_losses = test_model()
				test_score = numpy.mean(test_losses)
				print((('     epoch %i, minibatch %i/%i, test error of '
					   'best model %f %%') %
					  (epoch, minibatch_index + 1, n_train_batches,
					   test_score * 100.)))

		if patience <= iter:
			done_looping = True
			break

end_time = time.clock()
print((('Optimization complete with best validation score of %f %%,'
	   'with test performance %f %%') %
			 (best_validation_loss * 100., test_score * 100.)))
			 
# post analysis and plotting
cm = confusion_matrix(valid_set_y.eval().tolist(), prediction_out)

# Show confusion matrix in a separate window
pl.matshow(cm)
pl.title('Confusion matrix')
pl.colorbar()
pl.ylabel('True label')
pl.xlabel('Predicted label')
pl.show()


