import numpy
from joblib import Parallel, delayed

def Foo(i, data):
	print str(i)
	return i

def ParFoo():
	list_out = Parallel(n_jobs=-1, verbose=10)(delayed(Foo)(i, numpy.random.random((10000, 1000))) for i in range(5))

print 'run ParFoo'
ParFoo()
print 'done'
