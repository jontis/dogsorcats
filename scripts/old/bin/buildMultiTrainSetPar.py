#buildTrainSet
import glob, pandas, numpy, gzip, pickle
from progressbar import ProgressBar, Percentage, Bar, RotatingMarker, ETA
from joblib import Parallel, delayed
from scipy import io
from cmath import phase
from math import *
from sklearn.preprocessing import StandardScaler

# supress warning message that sklearn causes in pandas
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning, module="pandas", lineno=570)

# TODO: the background subtracter does not play well with appliances that are on on different days. fix.

scipy.ndimage.imread(fname, flatten=False, mode=None)

def CheckDataFilesAndParse(house, set_type='train'):
	house = str(house)
	# check for presence of the files, reparse if there are none
	data_path_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	data_path_source = '/home/jonathan/kaggleSlow/belkin/sourceData/'
	
	def ParseMatData(path):
		# Load the .mat files
		testData = io.loadmat(path)
		
		# Extract tables
		buf = testData['Buffer']
		v1 = buf['LF1V'][0][0]
		i1 = buf['LF1I'][0][0]
		v2 = buf['LF2V'][0][0]
		i2 = buf['LF2I'][0][0]
		t1 = buf['TimeTicks1'][0][0]
		t2 = buf['TimeTicks2'][0][0]
		hf = buf['HF'][0][0]
		thf = buf['TimeTicksHF'][0][0]
		out_lf = {'v1': v1, 'i1': i1, 'v2': v2, 'i2': i2, 't1': t1, 't2': t2}
		out_hf = {'hf': hf, 'thf': thf}
		return out_lf, out_hf
		
	
	def CombinePower(data, smooth=False):
		# PROCESSRAWBELKINDATA  Takes raw Current and Voltage harmonics to yield
		# Real, Reactive and Apparent Power along with Power Factor for each phase  
		t1 = data['t1'].flatten()
		t2 = data['t2'].flatten()
		v1, v2 = data['v1'], data['v2']
		i1, i2 = data['i1'], data['i2']
		
		def Shorten(t, v, i, from_start, ticks):
			if from_start: return t[ticks:], v[ticks:, :], i[ticks:, :]
			else: return t[:-ticks], v[:-ticks, :], i[:-ticks, :]
		
		
		#harmonize ticks now, the difference is so small that we just match lengths
		if len(t1) > len(t2):
			if t1[0] < t2[0]: t1, v1, i1 = Shorten(t1, v1, i1, from_start=True, ticks=(len(t1)-len(t2)))
			else: t1, v1, i1 = Shorten(t1, v1, i1, from_start=False, ticks=(len(t1)-len(t2)))
		elif len(t1) < len(t2):
			if t1[0] > t2[0]: t2, v2, i2 = Shorten(t2, v2, i2, from_start=True, ticks=(len(t2)-len(t1)))
			else: t2, v2, i2 = Shorten(t2, v2, i2, from_start=False, ticks=(len(t2)-len(t1)))
		t = t1 # setting time to t, t1 & t2 are supposed to be the same now
		#check lengths
		if abs(len(t1)-len(t2)) + abs(v1.shape[0] - v2.shape[0]) + abs(i1.shape[0]-i2.shape[0]) > 0:
			print 'length mathing failed'
		
		# Calculate power (by convolution)
		p1 = v1 * i1.conjugate()
		p2 = v2 * i2.conjugate()
		
		if smooth: # quick and dirty smooth with 5 points mean window
			p1 = (p1[:-4, :] + p1[1:-3, :] + p1[2:-2, :] + p1[3:-1, :] + p1[4:, :]) / 5
			p2 = (p2[:-4, :] + p2[1:-3, :] + p2[2:-2, :] + p2[3:-1, :] + p2[4:, :]) / 5
			t = t[2:-2]
		
		ComplexPower1 = p1.sum(axis=1)
		ComplexPower2 = p2.sum(axis=1)
		# Extract components
		p1_re = ComplexPower1.real
		p1_im = ComplexPower1.imag
		p1_app = abs(ComplexPower1)
		p2_re = ComplexPower2.real
		p2_im = ComplexPower2.imag
		p2_app = abs(ComplexPower2)
		
		l1_pf = pandas.np.asarray([cos(phase(p1[i,0])) for i in range(len(p1[:,0]))])
		l2_pf = pandas.np.asarray([cos(phase(p2[i,0])) for i in range(len(p2[:,0]))])
		
		t = numpy.floor(t) # floor to full seconds to match with HF
		
		df_out = pandas.DataFrame({'t': t, 
			'p1_re': p1_re, 'p1_im': p1_im, 'p1_app': p1_app, 'l1_pf': l1_pf, 
			'p2_re': p2_re, 'p2_im': p2_im, 'p2_app': p2_app, 'l2_pf': l2_pf})
		
		df_out = df_out.drop_duplicates(cols='t') # keep one every second to match HF
		return df_out
	
	
	def MergeHfToDataFrame(dict_hf, smooth=False, time_data=False):
		hf = dict_hf['hf'].transpose()
		thf = numpy.floor(dict_hf['thf']).flatten()
		if smooth:
			hf = (hf[:-4, :] + hf[1:-3, :] + hf[2:-2, :] + hf[3:-1, :] + hf[4:, :])/5
			thf = thf[2:-2]
		
		df_hf = pandas.DataFrame(hf)
		df_hf['t'] = thf
		if time_data:
			df_hf['month'] = pandas.to_datetime(thf[0], unit='s').month
			df_hf['wday'] = pandas.to_datetime(thf[0], unit='s').dayofweek
			df_hf['hour'] = pandas.to_datetime(thf, unit='s').hour
		df_hf = df_hf.drop_duplicates(cols='t')
		return df_hf
	
	
	def CheckForParsedFiles(set_type='train'):
		return sorted(glob.glob(data_path_gen + set_type +'_parsed_H' + house + '-?.h5'))
	
	
	def GetSourceDataFileNames(set_type='train'):
		if set_type=='train':
			return sorted(glob.glob(data_path_source + 'H' + house + '/'+ 'Tagged*.mat')) # only pick training files
		else:
			return sorted(glob.glob(data_path_source + 'H' + house + '/'+ 'Testing*.mat')) # only pick test files
	
	
	if (len(CheckForParsedFiles(set_type)) < 1):
		print 'found no files, parsing'
		data_files = GetSourceDataFileNames(set_type)
		for i in range(len(data_files)):
			print 'loading ' + data_files[i]
			dict_lf, dict_hf = ParseMatData(data_files[i])
			df_lf = CombinePower(dict_lf, smooth=False)
			df_hf = MergeHfToDataFrame(dict_hf, smooth=False)
			df_hflf = pandas.merge(df_hf, df_lf, on='t')
			df_hflf = df_hflf.reset_index() # reset index to work after append
			df_hflf = DfRemoveArtefacts(df_hflf)
			file_name = set_type + '_parsed_H' + house + '-' + str(i) + '.h5'
			print 'writing ' + data_path_gen + file_name
			df_hflf.to_hdf(data_path_gen + file_name, 'df')
	else:
		print 'found at least one file, not parsing'
	return CheckForParsedFiles(set_type)


def CombinePreparsedFiles(house, set_type='train'):
	raise NameError('This function is obsolete. Dont use it') 
	data_path_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	data_files = CheckDataFilesAndParse(house, set_type)
	df_hflf = pandas.DataFrame()
	for i in range(len(data_files)):
		print 'loading ' + data_files[i]
		df_hf = pandas.read_hdf(data_files[i], 'hf')
		df_lf = pandas.read_hdf(data_files[i], 'lf')
		df_hflf = df_hflf.append(pandas.merge(df_hf, df_lf, on='t'))
	df_hflf = df_hflf.reset_index() # reset index to work after append
	df_hflf = DfRemoveArtefacts(df_hflf)
	file_name = 'parsed_' + set_type + '_H' + house + '-raw.h5'
	print 'writing ' + data_path_gen + file_name
	df_hflf.to_hdf(data_path_gen + file_name, 'df')
	return df_hflf


def ParseLabels(house):
	raise NameError('not working right now, fix if you need it')
	#CombinePreparsedFiles(house)
	print 'start parsing labels house ' + house
	data_path_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	data_path_source = '/home/jonathan/kaggleSlow/belkin/sourceData/'
	name_file_load = 'H' + house + '/AllTaggingInfo.csv'
	print 'loading ' + data_path_source + name_file_load
	label_data = pandas.read_csv(data_path_source + name_file_load, header=None)
	appliance_label_key = label_data.drop_duplicates(cols=0).iloc[:,0:2]
	appliance_label_key = appliance_label_key.sort(columns=0)
	name_file_load = 'parsed_train_H' + house + '-raw.h5'
	print 'loading ' + data_path_source + name_file_load
	df_hflf = pandas.read_hdf(data_path_gen + name_file_load, 'df')
	ticks = df_hflf['t'].values
	del df_hflf
	appliance_ids = [0] + appliance_label_key[0].tolist()
	label_mask = numpy.zeros((len(ticks), len(appliance_ids)),  dtype=numpy.bool)
	for i in range(label_data.shape[0]):
		tick_mask = (ticks >= label_data.iloc[i,2]) & (ticks <= label_data.iloc[i,3])
		label_mask[tick_mask, label_data.iloc[i,0]] = 1
	df_label_mask = pandas.DataFrame(label_mask)
	df_label_mask['ticks'] = ticks
	name_file_save = 'labelsH' + house + '.h5'
	print 'writing ' + data_path_gen + name_file_save
	df_label_mask.to_hdf(data_path_gen + name_file_save, 'df')
	# pylab.imshow(df_label_mask.drop(['ticks'], axis=1).values, aspect='auto')
	# pylab.show()
	return df_label_mask


def DfRemoveArtefacts(df_data):
	while 'index' in df_data.keys(): df_data.pop('index') # remove artefacts
	while 'level_0' in df_data.keys(): df_data.pop('level_0') # remove artefacts
	return df_data


def BuildLabelIndex(i, df_labels, max_positives, max_negatives):
	if i > 0:
		if sum(df_labels.values[:,i]) > 0: # miminum positive examples to try this method
			pos_index = numpy.arange(df_labels.shape[0])[(df_labels.iloc[:, i] == 1)]
			pos_index = numpy.random.choice(pos_index, max_positives) # downsample / upsample to desired amount
			df_index_label = pandas.DataFrame({'data_index': pos_index, 'label': i})
			return df_index_label
	else:
		neg_index = numpy.arange(df_labels.shape[0])[(numpy.sum(df_labels.values, axis=1)==0)]
		neg_index = numpy.random.choice(neg_index, max_negatives) # downsample / upsample to desired amount
		df_index_label = pandas.DataFrame({'data_index': neg_index, 'label': 0})
		return df_index_label


def BuildBackgroundIndex(i, df_labels, df_index_label, background_span): # finds a span of background rows to subtract from measurements
	if i > 0:
		first_i_row = min(df_index_label[df_index_label['label']==i]['data_index'])
		bg_rows = df_labels.values[:first_i_row, :].sum(1) == 0 # find rows labeled background
		window_mean = pandas.rolling_window(numpy.flipud(bg_rows), window=background_span, win_type='boxcar')
		span_start_before_i = min(numpy.arange(len(bg_rows))[window_mean==1]) + 1
		first_bg_row = first_i_row - span_start_before_i
		return pandas.DataFrame([{'label': i, 'row': first_bg_row}])


def SubtractBackgroundFromLabeledSet(i, df_multi_train, df_bg_index, dict_bg_mean):
	if i > 0: df_multi_train = df_multi_train - dict_bg_mean[numpy.asscalar(df_bg_index[df_bg_index['label']==i]['row'])]
	return df_multi_train


def SubtractBackgroundFromLabeledIdSet(i, df_multi_train, df_bg_index, dict_bg_mean):
	if i > 0: df_multi_train = df_multi_train - dict_bg_mean[numpy.asscalar(df_bg_index[df_bg_index['id']==i]['row'])]
	return df_multi_train


def BuildMultiTrainSet(house):
	raise NameError('old version, use new instead')
	data_path_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	save_file = 'train_multi_H' + house + '-raw.h5'
	# check for file and bypass processing if there is a cached version
	if len(glob.glob(data_path_gen + save_file)) > 0:
		print 'found premade train set, skipping build'
		df_multi_train = pandas.read_hdf(data_path_gen + save_file, 'df')
		return df_multi_train
	
	ParseLabels(house)
	print 'start building multi train set house ' + house
	label_file = 'labelsH' + house + '.h5'
	df_labels = pandas.read_hdf(data_path_gen + label_file, 'df')
	df_ticks = df_labels.pop('ticks')
	
	max_positives = 500
	max_negatives = 500
	background_span = 500
	n_label_ids = df_labels.shape[1]
	data_file = 'parsed_train_H' + house + '-raw.h5'
	df_data = pandas.read_hdf(data_path_gen + data_file, 'df')
	
	list_df_label_index = Parallel(n_jobs=-1, verbose=5)( # parallell loop
		delayed(BuildLabelIndex)(
			i, df_labels, max_positives, max_negatives)
			for i in range(n_label_ids))
	
	df_index_label = pandas.concat(list_df_label_index)
	
	if background_span > 0: # find contiguos set of background closest prior to the label sample
		list_df_bg_index = Parallel(n_jobs=-1, verbose=5)( # parallell loop
			delayed(BuildBackgroundIndex)(
				i, df_labels, df_index_label, background_span)
				for i in range(1, n_label_ids))
	
	df_bg_index = pandas.concat(list_df_bg_index)
	# extract backgrounds from measurements
	# check for failures here? sometimes 'p' is over 2000 while nothing is on, automatic heaters / ac?
	dict_bg_mean = {}
	for i in set(df_bg_index['row']):
		bg_mean = df_data.iloc[i:i+background_span, :].drop('t',1).mean(0)
		bg_mean = bg_mean.append(pandas.Series([0, 0], index=['t', 'label'])) # append 't' + 'label' col to be able to just subtract from data rows
		dict_bg_mean[i] = bg_mean
	
	df_multi_train = df_data.iloc[df_index_label['data_index'], :]
	del df_data # cleanup
	df_multi_train['label'] = df_index_label['label'].values

	#return df_bg_index, dict_bg_mean
	print 'starting parallell loop'
	# subtract backgrounds
	list_df_multi_train = Parallel(n_jobs=1, verbose=10)( # parallell loop
		delayed(SubtractBackgroundFromLabeledSet)(
			i, df_multi_train[df_multi_train['label']==i], df_bg_index, dict_bg_mean)
			for i in range(n_label_ids))
	
	df_multi_train = pandas.concat(list_df_multi_train)
	df_multi_train = df_multi_train.reset_index() # reset index after appends
	df_multi_train = DfRemoveArtefacts(df_multi_train)
	#df_multi_train = df_multi_train.astype('float32') # downcast for size and CUDA
	df_multi_train.to_hdf(data_path_gen + save_file, 'df')
	print 'done house ' + house
	return df_multi_train


def BuildBackgroundIdIndex(i, df_day_train_index, background_span): # finds a span of background rows to subtract from measurements
	if i > 0:
		first_i_row = min(df_day_train_index[df_day_train_index['id']==i].index)
		bg_rows = (df_day_train_index['select'] == 0)[:first_i_row].values # find rows labeled background
		window_mean = pandas.rolling_window(numpy.flipud(bg_rows), window=background_span, win_type='boxcar')
		span_start_before_i = min(numpy.arange(len(bg_rows))[window_mean==1]) + 1
		first_bg_row = first_i_row - span_start_before_i
		return pandas.DataFrame([{'id': i, 'row': first_bg_row}])
	
	
def BuildMultiTrainSetNew(house):
	path_data_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	path_data_source = '/home/jonathan/kaggleSlow/belkin/sourceData/'
	name_file_load = 'H' + house + '/AllTaggingInfo.csv'
	print 'loading ' + path_data_source + name_file_load
	df_label_data = pandas.read_csv(path_data_source + name_file_load, header=None)
	df_label_data.columns = ['label', 'appl', 'start', 'stop']
	df_label_data = df_label_data.sort('start')
	df_label_data = df_label_data.reset_index(drop=True)
	df_label_data['id'] = df_label_data.index + 1

	name_save_file = 'train_multi_H' + house + '.h5'
	# check for file and bypass processing if there is a cached version
	if len(glob.glob(path_data_gen + name_save_file)) > 0:
		print 'found premade train set, skipping build'
		df_multi_train = pandas.read_hdf(path_data_gen + name_save_file, 'df')
		return df_multi_train
	
	max_positives = 500
	max_negatives = 500
	background_span = 500
	
	list_data_files = CheckDataFilesAndParse(house, 'train')
	for i, pathname in enumerate(list_data_files): # operate on one day of training data
		df_data = pandas.read_hdf(pathname, 'df') # load one day of training data
		t_day = df_data.t.values
		#find the days labels
		select_label_rows = (df_label_data['start'] >= min(t_day)) & (df_label_data['stop'] <= max(t_day))
		df_labels_day = df_label_data[select_label_rows] # iterate label rows
		df_day_train_index = pandas.DataFrame({'t': t_day, 'label': 0, 'select': False, 'id': 0, 'bg_row': 0})
		for j, label_row in df_labels_day.iterrows(): # iterate through the day's labels
			select_data_rows = (t_day >= label_row['start']) & (t_day <= label_row['stop'])
			df_day_train_index.ix[select_data_rows, 'label'] = label_row['label']
			df_day_train_index.ix[select_data_rows, 'select'] = True
			df_day_train_index.ix[select_data_rows, 'id'] = j
			
		#return df_day_train_index, df_data, df_labels_day
		if background_span > 0: # find contiguos set of background closest prior to the label sample
			list_df_bg_index = Parallel(n_jobs=-1, verbose=0)( # parallell loop
				delayed(BuildBackgroundIdIndex)(
					aa, df_day_train_index, background_span)
					for aa in set(df_day_train_index['id']))
		df_bg_index = pandas.concat(list_df_bg_index)
		
		# extract backgrounds from measurements
		# check for failures here? sometimes 'p' is over 2000 while nothing is on, automatic heaters / ac?
		dict_bg_mean = {}
		for k in set(df_bg_index['row']):
			bg_mean = df_data.iloc[k:k+background_span, :].drop('t',1).mean(0)
			bg_mean = bg_mean.append(pandas.Series([0, 0, 0], index=['t', 'label', 'id'])) # append 't', 'label', 'id' col to be able to just subtract from data rows
			dict_bg_mean[k] = bg_mean
		
		df_data['label'] = df_day_train_index['label'].values
		df_data['id'] = df_day_train_index['id'].values
		df_multi_train = df_data[df_day_train_index['select']]
		
		#pull a sample of negatives
		neg_index = numpy.arange(len(t_day))[df_day_train_index['select'] == False]
		neg_index = numpy.random.choice(neg_index, max_negatives) # downsample to desired amount
		df_negatives = df_data.iloc[neg_index, :] 
		bg_neg_mean = df_data.iloc[neg_index, :].mean(0)
		bg_neg_mean['t'] = 0
		df_negatives -= bg_neg_mean
		df_negatives['label'] = 0
		df_negatives['id'] = 0
		
		df_index_label = pandas.DataFrame({'data_index': neg_index, 'label': 0})
		del df_data # cleanup
		# subtract backgrounds
		list_df_multi_train = Parallel(n_jobs=-1, verbose=0)( # parallell loop
			delayed(SubtractBackgroundFromLabeledIdSet)(
				l, df_multi_train[df_multi_train['id']==l], df_bg_index, dict_bg_mean)
				for l in set(df_multi_train['id']))
		
		df_multi_train = pandas.concat(list_df_multi_train)
		df_multi_train = df_multi_train.append(df_negatives)
		df_multi_train = df_multi_train.reset_index(drop=True) # reset index after appends
		name_save_file = 'train_index_H' + house + '-' + str(i) + '.h5'
		print 'saving ' + name_save_file
		df_day_train_index.to_hdf(path_data_gen + name_save_file, 'df')
		name_save_file = 'train_multi_H' + house + '-' + str(i) + '.h5'
		print 'saving ' + name_save_file
		df_multi_train.to_hdf(path_data_gen + name_save_file, 'df')
	
	# concat all
	df_train_index = df_multi_train = pandas.DataFrame()
	for i in range(len(list_data_files)):
		name_load_file = 'train_index_H' + house + '-' + str(i) + '.h5'
		print 'loading ' + name_load_file
		df_day_train_index = pandas.read_hdf(path_data_gen + name_save_file, 'df')
		df_train_index = df_train_index.append(df_day_train_index)
		name_load_file = 'train_multi_H' + house + '-' + str(i) + '.h5'
		print 'loading ' + name_load_file
		df_day_multi_train = pandas.read_hdf(path_data_gen + name_load_file, 'df')
		df_multi_train = df_multi_train.append(df_day_multi_train)
	df_train_index = df_train_index.reset_index(drop=True) # reset index after appends
	df_multi_train = df_multi_train.reset_index(drop=True) # reset index after 
	name_save_file = 'train_index_H' + house + '.h5'
	print 'saving ' + name_save_file
	df_day_train_index.to_hdf(path_data_gen + name_save_file, 'df')
	name_save_file = 'train_multi_H' + house + '.h5'
	print 'saving ' + name_save_file
	df_multi_train.to_hdf(path_data_gen + name_save_file, 'df')
	#df_multi_train = df_multi_train.astype('float32') # downcast for size and CUDA
	print 'done house ' + house
	return df_multi_train


def BuildTestSet(house):
	raise NameError('thi function is obsolete, use the new function instead')
	path_data_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	name_save_file = 'test_H' + house + '.h5'
	# check for file and bypass processing if there is a cached version
	if len(glob.glob(path_data_gen + name_save_file)) > 0:
		print 'found premade test set, skipping build'
		df_test = pandas.read_hdf(path_data_gen + name_save_file, 'df')
		return df_test
	print 'start building test set ' + house
	df_sub = pandas.read_csv(path_data_gen + 'SampleSubmission.csv')
	print 'doing house ' + house
	target_appl =  set(df_sub[df_sub['House'] == 'H' + house]['Appliance'])
	target_ticks = numpy.array(sorted([x for x in set(df_sub[df_sub['House'] == 'H' + house]['TimeStamp'])]))
	CombinePreparsedFiles(house, set_type='test')
	data_file = 'parsed_test_H' + house + '-raw.h5'
	df_data = pandas.read_hdf(path_data_gen + data_file, 'df')
	df_data['t'] = df_data['t'].astype(int) # int the t column before you make it an index or your selections with int will go bad
	df_data.set_index(df_data['t'], inplace=True)
	df_data = DfRemoveArtefacts(df_data)
	df_test = df_data.loc[target_ticks]
	new_target_ticks = target_ticks
	while sum(numpy.isnan(df_test.iloc[:,0])) > 0: # check for missing rows, meaning keys were missing in the index
		t_miss_loc = numpy.isnan(df_test.iloc[:,0])
		print 'adjusted missed target ticks ' + str(sum(t_miss_loc))
		new_target_ticks[t_miss_loc] = new_target_ticks[t_miss_loc]+1 # try to get the ticks after
		df_test = df_data.loc[new_target_ticks]
		df_test['t'] = target_ticks
	
	df_test.set_index(df_test['t'], inplace=True)
	df_test = DfRemoveArtefacts(df_test)
	#df_test = df_test.astype('float32') # downcast for size and CUDA
	df_test.to_hdf(path_data_gen + name_save_file, 'df')
	print 'done house ' + house
	return df_test


def BuildTestSetNew(house):
	path_data_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	name_save_file = 'test_H' + house + '.h5'
	# check for file and bypass processing if there is a cached version
	if len(glob.glob(path_data_gen + name_save_file)) > 0:
		print 'found premade test set, skipping build'
		df_test = pandas.read_hdf(path_data_gen + name_save_file, 'df')
		return df_test
	print 'start building test set ' + house
	df_sub = pandas.read_csv(path_data_gen + 'SampleSubmission.csv')
	print 'doing house ' + house
	target_appl =  set(df_sub[df_sub['House'] == 'H' + house]['Appliance'])
	target_ticks = numpy.array(sorted([x for x in set(df_sub[df_sub['House'] == 'H' + house]['TimeStamp'])]))
	background_span = 500
	df_test = pandas.DataFrame()
	list_data_files = CheckDataFilesAndParse(house, 'test')
	for i, pathname in enumerate(list_data_files): # operate on one day of test data
		df_data = pandas.read_hdf(pathname, 'df')
		t_day = df_data.t.values
		target_ticks_day = target_ticks[(target_ticks >= min(t_day)) & (target_ticks <= max(t_day))]
		df_data['t'] = df_data['t'].astype(int) # int the t column before you make it an index or your selections with int will go bad
		df_data.set_index(df_data['t'], inplace=True)
		df_data = DfRemoveArtefacts(df_data)
		df_test_day = df_data.loc[target_ticks_day]
		new_target_ticks = target_ticks_day
		while sum(numpy.isnan(df_test_day.iloc[:,0])) > 0: # check for missing rows, meaning keys were missing in the index
			t_miss_loc = numpy.isnan(df_test_day.iloc[:,0])
			print 'adjusted missed target ticks ' + str(sum(t_miss_loc))
			new_target_ticks[t_miss_loc] = new_target_ticks[t_miss_loc]+1 # try to get the ticks after
			df_test_day = df_data.loc[new_target_ticks]
		df_test_day['t'] = target_ticks_day
		# remove background, hazard a guess at a background at 20000 ticks in the day for 06:00
		bg_start = 20000
		bg_mean = df_data.iloc[bg_start:bg_start + background_span, :].mean(0)
		bg_mean['t'] = 0
		# plain removal of the same for the whole day
		df_test_day -= bg_mean
		df_test = df_test.append(df_test_day)
	
	df_test.set_index(df_test['t'], inplace=True)
	df_test = DfRemoveArtefacts(df_test)
	#df_test = df_test.astype('float32') # downcast for size and CUDA
	df_test.to_hdf(path_data_gen + name_save_file, 'df')
	print 'done house ' + house
	return df_test



def PreProcessData(house, set_type='train', df_data=None):
	house = str(house)
	data_path_gen = '/home/jonathan/kaggle/belkin/generatedData/'
	if df_data is None: 
		if set_type=='train': 
			df_data = BuildMultiTrainSetNew(house)
		else:
			df_data = BuildTestSetNew(house)
	print 'preprocessing'
	# preprocess, scenter and scale
	p_file_name = 'preprocessorH' + house +'.pkl'
	df_ticks = df_data.pop('t') # don't preprocess ticks
	if set_type=='train': 
		labels = df_data.pop('label').values
		ids = df_data.pop('id').values
		myscale = StandardScaler()
		df_data = pandas.DataFrame(myscale.fit_transform(df_data), index=df_data.index, columns=df_data.columns)
		df_data['label'] = labels
		df_data['id'] = ids
		preprocessor_file = open(data_path_gen + p_file_name, 'wb') # pickle the preprocessor to be identical when processing test data
		pickle.dump(myscale, preprocessor_file)
		preprocessor_file.close()
	else:
		preprocessor_file = open(data_path_gen + p_file_name, 'rb') # load the preprocessor
		myscale = pickle.load(preprocessor_file)
		preprocessor_file.close()
		df_data = pandas.DataFrame(myscale.transform(df_data), index=df_data.index, columns=df_data.columns)
	df_data = df_data.astype('float32') # downcast for size and CUDA
	df_data['t'] = df_ticks
	
	print df_data
	if set_type=='train':
		name_save_file = 'train_multi_H' + house + '-full.h5'
		print 'writing to ' + data_path_gen + name_save_file
		df_data.to_hdf(data_path_gen + name_save_file, 'df')
	else:
		name_save_file = 'test_H' + house + '-full.h5'
		print 'writing to ' + data_path_gen + name_save_file
		df_data.to_hdf(data_path_gen + name_save_file, 'df')


if __name__ == '__main__':
	PreProcessData(1, 'train')
	PreProcessData(2, 'train')
	PreProcessData(3, 'train')
	PreProcessData(4, 'train')
	
	PreProcessData(1, 'test')
	PreProcessData(2, 'test')
	PreProcessData(3, 'test')
	PreProcessData(4, 'test')

"""
  # leftover code

  # Compute Power
  # Calculate the real and imaginary power at each harmonic content
  l1p.all = data$lfv1 * Conj(data$lfi1)
  l2p.all = data$lfv2 * Conj(data$lfi2)
   
  # Real, Reactive, Apparent powers, modul
  l1re.all = Re(l1p.all)
  l1im.all = Im(l1p.all)
  l1app.all = abs(l1p.all)
  l1arg.all = Arg(l1p.all)
  l1pf.all = cos(l1arg.all)
  l1re.sum = Re(l1p.sum)
  l1im.sum = Im(l1p.sum)
  l1app.sum = abs(l1p.sum)
  l1arg.sum = Arg(l1p.sum)
  l1pf.sum = cos(l1arg.sum)
  l2re.all = Re(l2p.all)
  l2im.all = Im(l2p.all)
  l2app.all = abs(l2p.all)
  l2arg.all = Arg(l2p.all)
  l2pf.all = cos(l2arg.all)
  l2re.sum = Re(l2p.sum)
  l2im.sum = Im(l2p.sum)
  l2app.sum = abs(l2p.sum)
  l2arg.sum = Arg(l2p.sum)
  l2pf.sum = cos(l2arg.sum)  

"""
