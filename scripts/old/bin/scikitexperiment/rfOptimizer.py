import numpy as np

from time import time
from operator import itemgetter
from scipy.stats import randint as sp_randint

from sklearn.grid_search import GridSearchCV, RandomizedSearchCV
from sklearn.datasets import load_digits
from sklearn.ensemble import RandomForestClassifier

# get some data

# build a classifier
# best parameters from validations
clf = RandomForestClassifier(n_estimators=100, n_jobs=-1, criterion = "entropy", max_depth = None,
                             bootstrap = False, min_samples_leaf = 3, min_samples_split = 5,  max_features = 100)


# Utility function to report best scores
def report(grid_scores, n_top=10):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print "Model with rank: {0}".format(i + 1)
        print "Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores))
        print "Parameters: {0}".format(score.parameters)
        print ""


# specify parameters and distributions to sample from
param_dist = {"n_estimators" : 100,
              "criterion": ["gini", "entropy"]}

# run randomized search
n_iter_search = 100
random_search = RandomizedSearchCV(clf, param_distributions=param_dist,
                                   n_iter=n_iter_search, verbose=1)

start = time()
random_search.fit(X, y)
print "RandomizedSearchCV took %.2f seconds for %d candidates parameter settings." % ((time() - start), n_iter_search)
report(random_search.grid_scores_)

# use a full grid over all parameters
param_grid = {"n_estimators" : [10, 50, 100, 200]}

# run grid search
grid_search = GridSearchCV(clf, param_grid=param_grid, verbose=10)
start = time()
grid_search.fit(X, y)

print "GridSearchCV took %.2f seconds for %d candidate parameter settings." % (time() - start, len(grid_search.grid_scores_))
report(grid_search.grid_scores_)
