#!/usr/bin/env python3

import gzip, numpy, pickle, pandas

train_set_data = pandas.read_pickle('mnist_train_data').as_matrix()
train_set_labels = pandas.read_pickle('mnist_train_labels').as_matrix().flatten()
train_set = (train_set_data, train_set_labels)

valid_set_data = pandas.read_pickle('mnist_valid_data').as_matrix()
valid_set_labels = pandas.read_pickle('mnist_valid_labels').as_matrix().flatten()
valid_set = (valid_set_data, valid_set_labels)

test_set_data = pandas.read_pickle('mnist_test_data').as_matrix()
test_set_labels = pandas.read_pickle('mnist_test_labels').as_matrix().flatten()
test_set = (test_set_data, test_set_labels)

f = gzip.open('mnistPy3.pkl.gz', 'wb')
pickle.dump([train_set, valid_set, test_set], f) 
f.close()