#!/usr/bin/env python

import gzip, numpy, cPickle, pandas

f = gzip.open('mnist.pkl.gz', 'rb')
train_set, valid_set, test_set = cPickle.load(f)
f.close()

train_set_data = train_set[0]
train_set_labels = train_set[1]
train_set_data = pandas.DataFrame(train_set_data)
train_set_data.save('mnist_train_data')
train_set_labels = pandas.DataFrame(train_set_labels)
train_set_labels.save('mnist_train_labels')
train_set, train_set_data, train_set_labels = None, None, None

valid_set_data = valid_set[0]
valid_set_labels = valid_set[1]
valid_set_data = pandas.DataFrame(valid_set_data)
valid_set_data.save('mnist_valid_data')
valid_set_labels = pandas.DataFrame(valid_set_labels)
valid_set_labels.save('mnist_valid_labels')
valid_set, valid_set_data, valid_set_labels = None, None, None

test_set_data = test_set[0]
test_set_labels = test_set[1]
test_set_data = pandas.DataFrame(test_set_data)
test_set_data.save('mnist_test_data')
test_set_labels = pandas.DataFrame(test_set_labels)
test_set_labels.save('mnist_test_labels')
test_set, test_set_data, test_set_labels = None, None, None
