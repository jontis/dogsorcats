require('caret')
require('randomForest')
#require('RRF')              # regularized random forest
#require('obliqueRF')        # oblique random forest (binary?)
require('glmnet')            # generalised linear model
require('gbm')
require('e1071')             # support vector machine
require('nnet')              # neural net
require('pROC')              # ROC + AUC
if (config$svm$use.gpu) require('rpud') # these models run on the GPU and require nvidia CUDA > 1.3 hardware


#additional AIs that seem worth trying: convolutional neural nets, KNN
#hclust hierarcical clustering, kmean clustering, ksvm, ada stochastic boosting
#possibly useful packages: sprint
#rattle

AiOne <- function(config, ai_i, cause=NULL, effect=NULL, model=NULL, newcause=NULL, actions=NULL, ...){
  ##call single ai, actions can be:'coef', 'error', 'model', 'predict'
  ais[[ai_i]](config=config[[ai_i]], cause, effect, model, newcause, actions, ...)
}


ais <- list()                          #ais, can have actions: coef, error, model, predict
#coef returns highest found correlation with name, for finding successful features
#error returns CV errors in prediction (needs bool error too)
#predict returns predictions for newcause


kfoldCrossvalidate <- function(cause, effect, ai_fit, ai_predict, kfolds=5, error.type='auc', trace=F){
  # TODO: make this return the predictions in the correct order
  if(trace) message('tracing k-fold crossvalidation')
  kfold_index <- createFolds(1:nrow(cause), k=kfolds)
  this_kfold_error <- rep(as.numeric(NA), kfolds)
  for(k in 1:kfolds){
    if(trace) message('fold ', k, ' of ', kfolds)
    kfold_index_train <- as.vector(unlist(kfold_index[-k]))
    cause_train_k <- cause[kfold_index_train,]
    effect_train_k <- effect[kfold_index_train,]
    
    kfold_index_test <- as.vector(unlist(kfold_index[k]))
    cause_test_k <- cause[kfold_index_test,]
    effect_test_k <- effect[kfold_index_test,]
    
    fit <- ai_fit(cause_train_k, effect_train_k)
    prediction <- ai_predict(fit, cause_test_k)
    
    if(error.type=='abs_lin'){                                                     #absolute linear error      
      if(is.factor(prediction)){prediction <- as.numeric(as.vector(prediction))}
      if(is.factor(effect_test_k)){effect_test_k <- as.numeric(as.vector(effect_test_k))}
      this_kfold_error[k] <- mean(abs(effect_test_k - prediction))                                       
    }
    if(error.type=='sqr'){                                                         #squared error     
      if(is.factor(prediction)){prediction <- as.numeric(as.vector(prediction))}
      if(is.factor(effect_test_k)){effect_test_k <- as.numeric(as.vector(effect_test_k))}
      this_kfold_error[k] <- mean((effect_test_k - prediction)^2)                                       
    }
    if(error.type=='kaggle_Rsquared'){
      if(is.factor(prediction)){prediction <- as.numeric(as.vector(prediction))}
      if(is.factor(effect_test_k)){effect_test_k <- as.numeric(as.vector(effect_test_k))}
      this_kfold_error[k] <- error_measures$kaggle_Rsquared(effect_test_k, prediction)
    }
    if(error.type=='auc'){
      if(is.factor(prediction)){prediction <- as.numeric(as.vector(prediction))}
      if(is.factor(effect_test_k)){effect_test_k <- as.numeric(as.vector(effect_test_k))}
      this_kfold_error[k] <- as.numeric(auc(roc(effect_test_k, prediction)))
    }
    if(trace) message(error.type, ': ', this_kfold_error[k])
    
  }
  out <- mean(this_kfold_error)
  names(out) <- error.type
  out
}

kfoldPredict <- function(cause, effect, aiFit, aiPredict, kfolds=5, trace=F){
  # TODO: make this return the predictions in the correct order
  if(trace) message('tracing k-fold crossvalidation')
  kfold.index <- createFolds(1:nrow(cause), k=kfolds)
  predictions <- list()
  for(k in 1:kfolds){
    if(trace) message('fold ', k, ' of ', kfolds)
    kfold.index.train <- as.vector(unlist(kfold.index[-k]))
    cause.train.k <- cause[kfold.index.train,]
    effect.train.k <- effect[kfold.index.train,]
    
    kfold.index.test <- as.vector(unlist(kfold.index[k]))
    cause.test.k <- cause[kfold.index.test,]
    effect.test.k <- effect[kfold.index.test,]
    
    fit <- aiFit(cause.train.k, effect.train.k)
    predictions[[k]] <- data.frame(index=kfold.index.test, predict=aiPredict(fit, cause.test.k))
  }
  out <- rbind.fill(predictions)
  out <- out[order(out$index), 2] # only send out predictions in correct order
  out
}


error_measures <- list()

error_measures$kaggle_Rsquared <- function(x,y) {
  # is this the same as stats::cor()  ?
  # Returns R-squared.
  # R2 = \frac{[\sum_i(x_i-\bar x)(y_i-\bar y)]^2}{\sum_i(x_i-\bar x)^2 \sum_j(y_j-\bar y)^2}
  # Arugments: x = solution activities
  #            y = predicted activities
  
  if ( length(x) != length(y) ) {
    warning("Input vectors must be same length!")
  }
  else {
    avx <- mean(x) 
    avy <- mean(y)
    num <- sum( (x-avx)*(y-avy) )
    num <- num*num
    denom <- sum( (x-avx)*(x-avx) ) * sum( (y-avy)*(y-avy) )
    return(num/denom)
  }
}

ais$blank <- function (config, cause=NULL, effect=NULL, model=NULL, newcause=NULL, actions=NULL, trained=F, fake=F, ...) {  #blank ai for testing
  out <- list()
  if(actions %in% c('coef','error','model','predict')){
    if(actions %in% c('coef', 'model','predict')){
      if(trained) out$trained <- 'blank trained model'
      if(actions %in% 'coef') out$coef <- as.matrix(c(0,0,0,0,0))          #return top 5 largest coef to show a hit
      if(actions %in% 'model') out$model <- 'blank model' 
      if(actions %in% 'predict') out$predict <- 'blank predict'
    }
    if(actions %in% 'error') out$error <- 'blank error'
  }
  out
}



ais$glm <- function (config, cause=NULL, effect=NULL, model=NULL, newcause=NULL, actions=NULL, alpha=NULL, lambda=NULL, trained=F, fake=F, ...) {
  out <- list()
  if(is.null(alpha) & (!is.null(config$alpha))) alpha <- config$alpha        #alpha=1 for LASSO penalty, use smaller ~.5-.2 for elastic net
  if(is.null(lambda) & (!is.null(config$lambda))) lambda <- config$lambda
  if((!is.null(cause)) & is.data.frame(cause)){ 
    if(names(cause)[1] == 'Date') cause[,1] <- as.numeric(cause[,1])         #manually convert Dates, they will not convert gracefully in the autoconversion later                        
    cause <- as.matrix(cause)                                                #convert data.frames to matrix, glmnet requires matrix
  }
  if((!is.null(newcause)) & is.data.frame(newcause)){ 
    if(names(newcause)[1] == 'Date') newcause[,1] <- as.numeric(newcause[,1]) #manually convert Dates, they will not convert gracefully in the autoconversion later                        
    newcause <- as.matrix(newcause)                                           #convert data.frames to matrix, glmnet requires matrix
  }
  
  if(config$enable.na.roughfix){
    #TODO: check time consumption for tests and na.roughfix
    if(names(cause)[1] == 'Date'){                                              #roughfix can't handle Date
      if(sum(is.na(cause)) > 0) cause[,2:ncol(cause)] <- na.roughfix(cause[,2:ncol(cause)])          #replace NAs with means / medians, probably obscures causality but needed for glmnet
      if(sum(is.na(newcause)) > 0) newcause[,2:ncol(cause)] <- na.roughfix(newcause[,2:ncol(cause)]) #replace NAs with means / medians, probably obscures causality but needed for glmnet      
    }else{
      if(sum(is.na(cause)) > 0) cause <- na.roughfix(cause)                     #replace NAs with means / medians, probably obscures causality but needed for glmnet
      if(sum(is.na(newcause)) > 0) newcause <- na.roughfix(newcause)            #replace NAs with means / medians, probably obscures causality but needed for glmnet
    }
  }
  
  #TODO: make sure boolean effects are called with appropriate methods in glmnet
  ai_fit <- function(cause, effect) glmnet(cause, effect, alpha=alpha, lambda=lambda)
  
  ai_predict <- function(fit, cause) predict(fit, cause, s=lambda)
  
  
  if(fake) cause <- cbind(effect, cause)                                           #fake it! effect added to cause column to test the ai
  
  if(actions %in% c('coef','error','model','predict')){
    if(actions %in% c('coef', 'model','predict')){
      if(is.null(alpha) | is.null(lambda)){
        trained.glmnet <- train.my$glmnet(config, cause, effect)
        alpha <- trained.glmnet$bestTune$.alpha
        lambda <- trained.glmnet$bestTune$.lambda
        if(trained) out$trained <- trained.glmnet
      } 

      if (is.null(model)) {fit <- ai_fit(cause, effect)}else{fit <- model}
      if(actions %in% 'coef') out$coef <- sort(abs(as.matrix(coef(fit)))[,1], decreasing=T)[1:10]         #return top 5 largest coef to show a hit
      if(actions %in% 'model') out$model <- fit 
      if(actions %in% 'predict') out$predict <- ai_predict(fit, newcause)
    }
    if(actions %in% 'error') out$error <- kfold_crossvalidate(cause, effect, ai_fit, ai_predict, error.type=config$error.type, ...)
    
  }
  out
}


ais$gbm <- function (config, cause=NULL, effect=NULL, model=NULL, newcause=NULL, actions=NULL, ...) {
  out <- list()
 
  ai.fit <- function(cause, effect) {
    if ((!is.null(effect)) & is.data.frame(effect)) effect <- unlist(effect) # reshape to fit the receiving func  
    gbm.fit(cause, effect, distribution='bernoulli',
            n.trees = ifelse(is.null(config$ntree), 100, config$ntree), 
            shrinkage = ifelse(is.null(config$shrinkage), 0.1, config$shrinkage),
            interaction.depth = ifelse(is.null(config$interaction.depth), 1, config$interaction.depth), 
            n.minobsinnode = ifelse(is.null(config$n.minobsinnode), 10, config$n.minobsinnode), 
            verbose=F)
  }
  
  ai.predict <- function(fit, cause) {
    #predict(fit, cause, n.trees=ifelse(is.null(config$ntree), 100, config$ntree))
    pred.gbm.resp <- predict(fit, cause, n.trees=ifelse(is.null(config$ntree), 100, config$ntree), type='response')
  }
    
  if(actions %in% c('coef','error','model','predict','cvpred')){
    if(actions %in% c('coef','model','predict')){
      if (is.null(model)) {fit <- ai.fit(cause, effect)}else{fit <- model}
      if(actions %in% 'coef') out$coef <- sort(fit$importance[,1], decreasing=T)[1:10]    #return top 5 largest coef to show a hit
      if(actions %in% 'model') out$model <- fit
      if(actions %in% 'predict') out$predict <- ai.predict(fit, newcause)
    }
    if(actions %in% 'error') out$error <- kfoldCrossvalidate(cause, effect, ai.fit, ai.predict, config$kfolds, config$error.type, ...)
    if(actions %in% 'cvpred') out$cvpred <- kfoldPredict(cause, effect, ai.fit, ai.predict, config$kfolds, ...)
  }
  out
}


ais$rf <- function (config, cause=NULL, effect=NULL, model=NULL, newcause=NULL, actions=NULL, par=F, ...) {
  out <- list()
  
  ai.fit <- function(cause, effect, nodesize=-1, maxnodes=-1, mtry=-1){
    if((!is.null(effect)) & is.data.frame(effect)) effect <- unlist(effect)           # reshape to fit receiver                   
    if((!is.null(effect)) & is.numeric(effect)) effect <- as.factor(effect)           # factor for decision                       
    if(is.null(config$ntree)){ntree <- 100}else{ntree <- config$ntree}
    if(is.null(config$nodesize)){nodesize <- 5}else{nodesize <- config$nodesize}      # set a higher minimum number for group size in nodes, limits memory use
    if(is.null(config$maxnodes)){maxnodes <- NULL}else{maxnodes <- config$maxnodes}   # set a limited max node number (experiment with this) to reduce tree size
    if(is.null(config$mtry)){mtry <- max(floor(ncol(cause)/3), 1)}else{mtry <- config$mtry}
    if(par){
      ais$rf.par(cause, effect, ntree=ntree, mtry=mtry, nodesize=nodesize, maxnodes=maxnodes, importance=F)           #parallel
    }else{
      randomForest(cause, effect, ntree=ntree, mtry=mtry, nodesize=nodesize, maxnodes=maxnodes, importance=F)   #single thread
    }
  }
  
  ai.predict <- function(fit, cause) predict(fit, cause, type='prob')[,2] # hardcoded to select prob for class '1'  

  if(actions %in% c('coef','error','model','predict','cvpred')){
    if(actions %in% c('coef','model','predict')){
      if (is.null(model)) {fit <- ai.fit(cause, effect)}else{fit <- model}
      if(actions %in% 'coef') out$coef <- sort(fit$importance[,1], decreasing=T)[1:10]    #return top 5 largest coef to show a hit
      if(actions %in% 'model') out$model <- fit
      if(actions %in% 'predict') out$predict <- ai.predict(fit, newcause)
    }
    if(actions %in% 'error') out$error <- kfoldCrossvalidate(cause, effect, ai.fit, ai.predict, error.type=config$error.type, ...)
    if(actions %in% 'cvpred') out$cvpred <- kfoldPredict(cause, effect, ai.fit, ai.predict, ...)
  }
  out
}


ais$rf.par <- function(cause, effect, ntree=100, ...){
  n <- 100
  N <- ntree %/% n
  #message('Im in rf.par, ntree = ', ntree, ', N = ', N)
  out <- foreach(i = 1:N, .export=c('cause', 'effect'), .combine=combine, .packages='randomForest', .inorder=FALSE) %dopar% {
    #bulletproof this, it may return something that is not an RF object
    randomForest(cause, effect, ntree=n, ...)
  }
  out
}


ais$svm <- function (config, ...) {
  if (config$use.gpu) {
    ais$gpusvm(config, ...)
  }else{
    ais$cpusvm(config, ...)
  }
}


ais$cpusvm <- function (config, cause=NULL, effect=NULL, model=NULL, newcause=NULL, actions=NULL, ...) {
  #WARNING: parameters of SVMs needs tuning to deliver sane results, this one is not tuned yet
  #TODO: make sure boolean effects are called with appropriate methods
  out <- list()
  
  aiFit <- function(cause, effect) {
    if((!is.null(effect)) & is.data.frame(effect)) effect <- unlist(effect)           # reshape to fit receiver                   
    if((!is.null(effect)) & is.numeric(effect)) effect <- as.factor(effect)           # factor for decision                       
    svm(cause, effect, 
        cost = ifelse(is.null(config$cost), 1, config$cost), 
        gamma = ifelse(is.null(config$kernel.width), 0.1, config$kernel.width),
        probability=TRUE)
  }
  
  aiPredict <- function(fit, cause) {
    pred.svm.class <- predict(fit, cause, probability = TRUE)
    out <- attr(pred.svm.class, "probabilities")[, 2] # output probability for bein class '1'
    out
  }
  
  if(actions %in% c('coef','error','model','predict','cvpred')){
    if(actions %in% c('coef','model','predict')){
      if (is.null(model)) {fit <- aiFit(cause, effect)}else{fit <- model}
      if(actions %in% 'model') out$model <- fit
      if(actions %in% 'predict') out$predict <- aiPredict(fit, newcause)
    }
    if(actions %in% 'error') out$error <- kfoldCrossvalidate(cause, effect, ai_fit, ai_predict, error.type=config$error.type, ...)
    if(actions %in% 'cvpred') out$cvpred <- kfoldPredict(cause, effect, aiFit, aiPredict, ...)
  }
  out
}


ais$nnet <- function (config, cause=NULL, effect=NULL, model=NULL, newcause=NULL, actions=NULL, ...) {
  #TODO: make sure that the right method is used for each case (classification / regression)
  #TODO: consider other measures of prediction error
  out <- list()
  
  aiFit <- function(cause, effect) {
    nnet(cause, effect, 
         size = ifelse(is.null(config$size), 100, config$size), 
         decay = ifelse(is.null(config$decay), 0, config$decay), 
         skip = ifelse(is.null(config$skip), FALSE, config$skip), 
         entropy = ifelse(is.null(config$entropy), FALSE, config$entropy),
         maxit=200, trace=FALSE, MaxNWts=100000)
  }
   
  aiPredict <- function(fit, cause) pred.nnet <- predict(fit, cause, type='raw')
    
  if(actions %in% c('coef','error','model','predict', 'cvpred')){
    if(actions %in% c('coef','model','predict')){
      if (is.null(model)) {fit <- aiFit(cause, effect)}else{fit <- model}
      if(actions %in% 'model') out$model <- fit
      if(actions %in% 'predict') out$predict <- aiPredict(fit, newcause)
    }
    if(actions %in% 'error') out$error <- kfoldCrossvalidate(cause, effect, aiFit, aiPredict, error.type=config$error.type, ...)
    if(actions %in% 'cvpred') out$cvpred <- kfoldPredict(cause, effect, aiFit, aiPredict, ...)
  }
  out
}


ais$glm_rf <- function (config, cause=NULL, effect=NULL, model=NULL, newcause=NULL, actions=NULL, alpha=NULL, lambda=NULL, ...) {
  out <- list()
  if(is.null(alpha) & (!is.null(config$alpha))) alpha <- config$alpha        #alpha=1 for LASSO penalty, use smaller ~.5-.2 for elastic net
  if(is.null(lambda) & (!is.null(config$lambda))) lambda <- config$lambda
  if(is.null(config$nodesize)){nodesize <- 5}else{nodesize <- config$nodesize}      #set a higher minimum number for group size in nodes, limits memory use
  if(is.null(config$maxnodes)){maxnodes <- NULL}else{maxnodes <- config$maxnodes}   #set a limited max node number (experiment with this) to reduce tree size
  if(is.null(config$mtry)){mtry <- max(floor(ncol(cause)/3), 1)}else{mtry <- config$mtry}
  
  ai_fit <- function(cause, effect, par=T){
    fit <- list()    
    fit$glm <- glmnet(cause, effect, alpha=alpha, lambda=lambda)
    cause <- cbind(predict(fit$glm, cause, s=lambda), cause)
    if(par){
      fit$rf <- ais$rf_par(cause, effect, ntree=config$ntree, mtry=mtry, nodesize=nodesize, maxnodes=maxnodes, importance=F)           #parallel
    }else{
      fit$rf <- randomForest(cause, effect, ntree=config$ntree, mtry=mtry, nodesize=nodesize, maxnodes=maxnodes, importance=F)   #single thread
    }
    fit
  } 
  
  ai_predict <- function(fit, cause){
    cause <- cbind(predict(fit$glm, cause,  s=lambda), cause)
    predict(fit$rf, cause)
  } 
  
  
  if(fake) cause <- cbind(effect, cause)                                           #fake it! effect added to cause column to test the ai
  
  if(coef | error | model){
    if(coef | model){
      if(is.null(alpha) | is.null(lambda)){
        #trained.glmnet <- train.my$glmnet(config, cause, effect)
        alpha <- trained.glmnet$bestTune$.alpha
        lambda <- trained.glmnet$bestTune$.lambda
        if(trained) out$trained <- trained.glmnet
      } 
      fit <- ai_fit(cause, effect)
      if(coef) out$coef <- sort(abs(as.matrix(coef(fit)))[,1], decreasing=T)[1:10]         #return top 5 largest coef to show a hit
      if(model) out$model <- fit 
    }
    if(error) out$error <- kfold_crossvalidate(cause, effect, ai_fit, ai_predict, error.type=config$error.type, ...)
    
  }
  out
}


ais$ensemble_glm_rf <- function (config, cause=NULL, effect=NULL, model=NULL, newcause=NULL, actions=NULL, alpha=NULL, lambda=NULL, weights=NULL, ...) {
  out <- list()
  if(is.null(alpha) & (!is.null(config$alpha))) alpha <- config$alpha        #alpha=1 for LASSO penalty, use smaller ~.5-.2 for elastic net
  if(is.null(lambda) & (!is.null(config$lambda))) lambda <- config$lambda
  if(is.null(config$nodesize)){nodesize <- 5}else{nodesize <- config$nodesize}      #set a higher minimum number for group size in nodes, limits memory use
  if(is.null(config$maxnodes)){maxnodes <- NULL}else{maxnodes <- config$maxnodes}   #set a limited max node number (experiment with this) to reduce tree size
  if(is.null(config$mtry)){mtry <- max(floor(ncol(cause)/3), 1)}else{mtry <- config$mtry}
  if(is.null(weights)){                                                             #if no mixing weights were suplpied, check config or use default
    if(is.null(config$weights_estimated)){
      weights <- c(1, 1)                                                            #mixing in equal parts if no weight is supplied
    }else{
      weights <- config$weights_estimated                                           #if no mixing weight were given directly, check default
    }
  } 
  
  
  ai_fit <- function(cause, effect, par=T){
    fit <- list()    
    fit$glm <- glmnet(cause, effect, alpha=alpha, lambda=lambda)
    if(par){
      fit$rf <- ais$rf_par(cause, effect, ntree=config$ntree, mtry=mtry, nodesize=nodesize, maxnodes=maxnodes, importance=F)           #parallel
    }else{
      fit$rf <- randomForest(cause, effect, ntree=config$ntree, mtry=mtry, nodesize=nodesize, maxnodes=maxnodes, importance=F)   #single thread
    }
    fit
  } 
  
  ai_predict <- function(fit, cause){
    if(is.null(weights)) weights <- rep(1, length(fit))                              #if no weighting is specified, use equal weights
    temp <- predict(fit$glm, cause,  s=lambda)
    temp <- cbind(temp, predict(fit$rf, cause))
    out <- apply(temp, MARGIN=1, FUN=function(row_i) weighted.mean(row_i, weights))  #weighted mixing of the ensemble
  } 
  
  
  if(fake) cause <- cbind(effect, cause)                                           #fake it! effect added to cause column to test the ai
  
  if(coef | error | model){
    if(coef | model){
      if(is.null(alpha) | is.null(lambda)){
        #trained.glmnet <- train.my$glmnet(config, cause, effect)
        alpha <- trained.glmnet$bestTune$.alpha
        lambda <- trained.glmnet$bestTune$.lambda
        if(train) out$trained <- trained.glmnet
      } 
      fit <- ai_fit(cause, effect)
      if(coef) out$coef <- sort(abs(as.matrix(coef(fit)))[,1], decreasing=T)[1:10]         #return top 5 largest coef to show a hit
      if(model) out$model <- fit 
    }
    if(error) out$error <- kfold_crossvalidate(cause, effect, ai_fit, ai_predict, error.type=config$error.type, ...)
    
  }
  out
}


ais$ensemble_glm_gbm_rf <- function (config, cause=NULL, effect=NULL, model=NULL, newcause=NULL, actions=NULL, alpha=NULL, lambda=NULL, weights=NULL, ...) {
  out <- list()
  if(is.null(alpha) & (!is.null(config$alpha))) alpha <- config$alpha        #alpha=1 for LASSO penalty, use smaller ~.5-.2 for elastic net
  if(is.null(lambda) & (!is.null(config$lambda))) lambda <- config$lambda
  if(is.null(config$nodesize)){nodesize <- 5}else{nodesize <- config$nodesize}      #set a higher minimum number for group size in nodes, limits memory use
  if(is.null(config$maxnodes)){maxnodes <- NULL}else{maxnodes <- config$maxnodes}   #set a limited max node number (experiment with this) to reduce tree size
  if(is.null(config$mtry)){mtry <- max(floor(ncol(cause)/3), 1)}else{mtry <- config$mtry}
  if(is.null(weights)){                                                             #if no mixing weights were suplpied, check config or use default
    if(is.null(config$weights_estimated)){
      weights <- c(1, 1, 1)                                                            #mixing in equal parts if no weight is supplied
    }else{
      weights <- config$weights_estimated                                           #if no mixing weight were given directly, check default
    }
  } 
  
  ai_fit <- function(cause, effect, par=T){
    fit <- list()    
    fit$glm <- glmnet(cause, effect, alpha=alpha, lambda=lambda)
    fit$gbm <- gbm.fit(cause, effect, distribution ="gaussian", n.trees = 100 , shrinkage = 0.1, interaction.depth = 2, verbose=F)
    if(par){
      fit$rf <- ais$rf_par(cause, effect, ntree=config$ntree, mtry=mtry, nodesize=nodesize, maxnodes=maxnodes, importance=F)           #parallel
    }else{
      fit$rf <- randomForest(cause, effect, ntree=config$ntree, mtry=mtry, nodesize=nodesize, maxnodes=maxnodes, importance=F)   #single thread
    }
    fit
  } 
  
  ai_predict <- function(fit, cause){
    if(is.null(weights)) weights <- rep(1, length(fit))                              #if no weighting is specified, use equal weights
    temp <- predict(fit$glm, cause,  s=lambda)
    temp <- cbind(temp, predict(fit$gbm, cause, n.trees = 100))
    temp <- cbind(temp, predict(fit$rf, cause))
    out <- apply(temp, MARGIN=1, FUN=function(row_i) weighted.mean(row_i, weights))  #weighted mixing of the ensemble
  } 
  
  
  if(fake) cause <- cbind(effect, cause)                                           #fake it! effect added to cause column to test the ai
  
  if(coef | error | model){
    if(coef | model){
      if(is.null(alpha) | is.null(lambda)){
        #trained.glmnet <- train.my$glmnet(config, cause, effect)
        alpha <- trained.glmnet$bestTune$.alpha
        lambda <- trained.glmnet$bestTune$.lambda
        if(train) out$trained <- trained.glmnet
      } 
      fit <- ai_fit(cause, effect)
      if(coef) out$coef <- sort(abs(as.matrix(coef(fit)))[,1], decreasing=T)[1:10]         #return top 5 largest coef to show a hit
      if(model) out$model <- fit 
    }
    if(error) out$error <- kfold_crossvalidate(cause, effect, ai_fit, ai_predict, error.type=config$error.type, ...)
    
  }
  out
}


ais$gpusvm <- function (config, cause=NULL, effect=NULL, model=NULL, newcause=NULL, actions=NULL, ...) {
  out <- list()
  
  aiFit <- function(cause, effect){
    if((!is.null(effect)) & is.data.frame(effect)) effect <- unlist(effect)           # reshape to fit receiver                   
    if((!is.null(effect)) & is.numeric(effect)) effect <- as.factor(effect)           # factor for decision                       
    sink('/dev/null')                                                           #invisible to supress output until rpud package matures
    out <- rpusvm(cause, effect, 
                  cost = ifelse(is.null(config$cost), 1, config$cost), 
                  gamma = ifelse(is.null(config$kernel.width), 0.1, config$kernel.width),
                  probability=TRUE)
    sink()
    out
  }
  
  aiPredict <- function(fit, cause) {
    sink('/dev/null')                                                           #invisible to supress output until rpud package matures
    pred.svm.class <- predict(fit, cause, probability = TRUE)
    sink()
    out <- attr(pred.svm.class, "probabilities")[, 2] # output probability for bein class '1'
    out
  }
  
  if(actions %in% c('coef','error','model','predict','cvpred')){
    if(actions %in% c('coef','model','predict')){
      if (is.null(model)) {fit <- aiFit(cause, effect)}else{fit <- model}
      if(actions %in% 'model') out$model <- fit
      if(actions %in% 'predict') out$predict <- aiPredict(fit, newcause)
    }
    if(actions %in% 'error') out$error <- kfold_crossvalidate(cause, effect, aiFit, aiPredict, error.type=config$error.type, ...)
    if(actions %in% 'cvpred') out$cvpred <- kfoldPredict(cause, effect, aiFit, aiPredict, ...)
  }
  out
}


predict.ensemble_glm_gbm_rf <- function(fit, cause, weights=NULL, lambda){
  if(is.null(weights)) weights <- rep(1, length(fit))                              #if no weighting is specified, use equal weights
  temp <- predict(fit$glm, cause,  s=lambda)
  temp <- cbind(temp, predict(fit$gbm, cause, n.trees = 150))
  temp <- cbind(temp, predict(fit$rf, cause))
  out <- apply(temp, MARGIN=1, FUN=function(row_i) weighted.mean(row_i, weights))  #weighted mixing of the ensemble
}



train.my <- list()


train.my$glmnet <- function(config, cause, effect, alpha=c(0,1), lambda=c(0,1), gridpoints=5, recurse_level=1) {
  if (!is.matrix(cause) | !is.matrix(effect)) message('please use matrix for cause and effect')
  effect <- factor(effect, labels=c('inte', 'val')) # needed for classificaton
  if(recurse_level==1) message('training glmnet')
  diff_a <- diff(alpha) / (gridpoints-1)
  diff_l <- diff(lambda) / (gridpoints-1)
  tuneGrid <- expand.grid(.alpha=seq(alpha[1], alpha[2], length.out=gridpoints), .lambda=seq(lambda[1], lambda[2], length.out=gridpoints))
  trained.glmnet <- train(cause, effect, method="glmnet", tuneGrid=tuneGrid, trControl=trainControl(verboseIter=F, classProbs=T))
  
  if(recurse_level<3){
    alpha <- c(max(alpha[1], trained.glmnet$bestTune$.alpha-diff_a), min(alpha[2], trained.glmnet$bestTune$.alpha+diff_a))
    message('present alpha: ', trained.glmnet$bestTune$.alpha, ', limits: ', alpha[1], ', ', alpha[2])
    lambda <- c(max(lambda[1], trained.glmnet$bestTune$.lambda-diff_l), min(lambda[2], trained.glmnet$bestTune$.lambda+diff_l))
    message('present lambda: ', trained.glmnet$bestTune$.lambda, ', limits: ', lambda[1], ', ', lambda[2])
    trained.glmnet <- train.my$glmnet(config, cause, effect, alpha, lambda, gridpoints, recurse_level=(recurse_level+1))
  }
  if(recurse_level==1) message('optimal glm values are alpha: ', trained.glmnet$bestTune$.alpha, ', lambda: ', trained.glmnet$bestTune$.lambda)
  trained.glmnet
}


train.my$rf <- function(config, cause, effect, mtry=c(50), nodesize=c(5), ntree=c(100, 200, 400)) {
  if (!is.matrix(cause) | !is.matrix(effect)) message('please use matrix for cause and effect')
  tuneGrid <- expand.grid(mtry=mtry, nodesize=nodesize, ntree=ntree)
  message('training rf, ', nrow(tuneGrid), ' items in search grid.')
  
  MyTuneRF <- function (config, cause, effect, tuneGrid) {
    errors <- NULL
    timers <- NULL
    for (i in 1:nrow(tuneGrid)) {
      params <- tuneGrid[i,]
      config$mtry <- params$mtry
      config$nodesize <- params$nodesize
      config$ntree <- params$ntree
      timer <- system.time(
        result <- ais$rf(config, cause, effect, actions=c('error'), kfolds=3)
      )[3]
      errors <- append(errors, result$error)
      timers <- append(timers, timer)
      message('mtry: ', params$mtry, ', nodesize: ', params$nodesize, ', trees: ', params$ntree, ', timer: ', round(timer), ', error: ', round(result$error, 4))
      if (result$error == max(errors)) {
        bestTune <- params
      }
    }
    grid.errors <- cbind(errors, timers, tuneGrid)
    grid.errors <- grid.errors[order(grid.errors$errors, decreasing=T),]
    out <- list(grid.errors=grid.errors, bestTune=bestTune)
    out
  }
  
  trained.rf <- MyTuneRF(config$svm, cause, effect, tuneGrid)
  trained.rf
}


train.my$nnet <- function(config, cause, effect, size=NULL, decay=NULL, gridpoints=5, recurse_level=1) {
  if (!is.matrix(cause) | !is.matrix(effect)) message('please use matrix for cause and effect')
  effect <- factor(effect, labels=c('inte', 'val')) # needed for classificaton
  stop() # not done yet
  
  fit <- train(training.set$train$data[-1], as.factor(unlist(training.set$train$data[1])), method='nnet', metric='Accuracy')
}


train.my$gbm <- function(config, cause, effect, shrinkage=c(0.1), interaction=c(5), ntrees=c(100, 1000), n.minobsinnode=c(10)) {
  if (!is.matrix(cause) | !is.matrix(effect)) message('please use matrix for cause and effect')
  tuneGrid <- expand.grid(shrinkage=shrinkage, interaction.depth=interaction, n.trees=ntrees, n.minobsinnode=n.minobsinnode)
  message('training gbm, ', nrow(tuneGrid), ' items in search grid.')
  
  MyTuneGBM <- function (config, cause, effect, tuneGrid) {
    errors <- NULL
    timers <- NULL
    for (i in 1:nrow(tuneGrid)) {
      params <- tuneGrid[i,]
      config$shrinkage <- params$shrinkage
      config$interaction.depth <- params$interaction.depth
      config$n.trees <- params$n.trees
      config$n.minobsinnode <- params$n.minobsinnode
      timer <- system.time(
        result <- ais$gbm(config, cause, effect, actions=c('error'), kfolds=3)
      )[3]
      errors <- append(errors, result$error)
      timers <- append(timers, timer)
      message('shrink: ', params$shrinkage, ', interaction: ', params$interaction.depth, ', trees: ', params$n.trees, ', minobsinnode: ', params$n.minobsinnode, ', timer: ', round(timer), ', error: ', round(result$error, 4))
      if (result$error == max(errors)) {
        bestTune <- params
      }
    }
    grid.errors <- cbind(errors, timers, tuneGrid)
    grid.errors <- grid.errors[order(grid.errors$errors, decreasing=T),]
    out <- list(grid.errors=grid.errors, bestTune=bestTune)
    out
  }
  
  trained.gbm <- MyTuneGBM(config$svm, cause, effect, tuneGrid)
  trained.gbm
}


train.my$svm <- function(config, cause, effect, kernel.width=c(6), cost=c(5), kernel=NULL) {
  if (!is.matrix(cause) | !is.matrix(effect)) message('please use matrix for cause and effect')
  #effect <- factor(effect, labels=c('inte', 'val')) # needed for classificaton
  cpu.or.gpu <- ifelse(config$svm$use.gpu, 'GPU', 'CPU')
  tuneGrid <- expand.grid(kernel.width=kernel.width, cost=cost)
  message('training svm ', cpu.or.gpu, ', ', nrow(tuneGrid), ' items in search grid.')
  
  MyTuneSVM <- function (config, cause, effect, tuneGrid) {
    errors <- NULL
    timers <- NULL
    for (i in 1:nrow(tuneGrid)) {
      params <- tuneGrid[i,]
      config$cost <- params$cost
      config$kernel.width=params$kernel.width
      timer <- system.time(
        result <- ais$svm(config, cause, effect, actions=c('error'), kfolds=5)
      )[3]
      errors <- append(errors, result$error)
      timers <- append(timers, timer)
      message('cost: ', params$cost, ', kernel.width: ', params$kernel.width, ', timer: ', round(timer), ', error: ', round(result$error, 4))
      if (result$error == max(errors)) {
        bestTune <- params
      }
    }
    grid.errors <- cbind(errors, timers, tuneGrid)
    grid.errors <- grid.errors[order(grid.errors$errors, decreasing=T),]
    out <- list(grid.errors=grid.errors, bestTune=bestTune)
    out
  }
  
  trained.svm <- MyTuneSVM(config$svm, cause, effect, tuneGrid)
  trained.svm
}


ErrorInvAuc <- function (effect, prediction) 1-as.numeric(auc(roc(effect, prediction)))