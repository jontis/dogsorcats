"""
Functionality for preprocessing Datasets.
"""

__authors__ = "Ian Goodfellow, David Warde-Farley, Guillaume Desjardins, and Mehdi Mirza"
__copyright__ = "Copyright 2010-2012, Universite de Montreal"
__credits__ = ["Ian Goodfellow", "David Warde-Farley", "Guillaume Desjardins",
               "Mehdi Mirza"]
__license__ = "3-clause BSD"
__maintainer__ = "Ian Goodfellow"
__email__ = "goodfeli@iro"

import time
import warnings
import numpy as np
from scipy import linalg


class Preprocessor(object):
    """
        Abstract class.

        An object that can preprocess a dataset.

        Preprocessing a dataset implies changing the data that
        a dataset actually stores. This can be useful to save
        memory--if you know you are always going to access only
        the same processed version of the dataset, it is better
        to process it once and discard the original.

        Preprocessors are capable of modifying many aspects of
        a dataset. For example, they can change the way that it
        converts between different formats of data. They can
        change the number of examples that a dataset stores.
        In other words, preprocessors can do a lot more than
        just example-wise transformations of the examples stored
        in the dataset.
    """

    def apply(self, dataset, can_fit=False):
        """
            dataset: The dataset to act on.
            can_fit: If True, the Preprocessor can adapt internal parameters
                     based on the contents of dataset. Otherwise it must not
                     fit any parameters, or must re-use old ones.
                     Subclasses should still have this default to False, so
                     that the behavior of the preprocessors is uniform.

            Typical usage:
                # Learn PCA preprocessing and apply it to the training set
                my_pca_preprocessor.apply(training_set, can_fit = True)
                # Now apply the same transformation to the test set
                my_pca_preprocessor.apply(test_set, can_fit = False)

            Note: this method must take a dataset, rather than a numpy ndarray,
                  for a variety of reasons:
                      1) Preprocessors should work on any dataset, and not all
                         datasets will store their data as ndarrays.
                      2) Preprocessors often need to change a dataset's metadata.
                         For example, suppose you have a DenseDesignMatrix dataset
                         of images. If you implement a fovea Preprocessor that
                         reduces the dimensionality of images by sampling them finely
                         near the center and coarsely with blurring at the edges,
                         then your preprocessor will need to change the way that the
                         dataset converts example vectors to images for visualization.
        """

        raise NotImplementedError(str(type(self))+" does not implement an apply method.")

    def invert(self):
        """
        Do any necessary prep work to be able to support the "inverse" method
        later. Default implementation is no-op.
        """

class ZCA(Preprocessor):
    """
    Performs ZCA whitening.
    TODO: add reference
    """
    def __init__(self, n_components=None, n_drop_components=None,
                 filter_bias=0.1):
        """
        n_components: TODO: WRITEME
        n_drop_components: TODO: WRITEME
        filter_bias: Filters are scaled by 1/sqrt(filter_bias + variance)
                    TODO: verify that default of 0.1 is what was used in the
                          Coates and Ng paper, add reference
        """
        warnings.warn("This ZCA preprocessor class is known to yield very "
                      "different results on different platforms. If you plan "
                      "to conduct experiments with this preprocessing on "
                      "multiple machines, it is probably a good idea to do "
                      "the preprocessing on a single machine and copy the "
                      "preprocessed datasets to the others, rather than "
                      "preprocessing the data independently in each "
                      "location.")
        # TODO: test to see if differences across platforms
        # e.g., preprocessing STL-10 patches in LISA lab versus on
        # Ian's Ubuntu 11.04 machine
        # are due to the problem having a bad condition number or due to
        # different version numbers of scipy or something
        self.n_components = n_components
        self.n_drop_components = n_drop_components
        self.copy = True
        self.filter_bias = filter_bias
        self.has_fit_ = False

    def fit(self, X):
        assert X.dtype in ['float32', 'float64']
        assert not np.any(np.isnan(X))
        assert len(X.shape) == 2
        n_samples = X.shape[0]
        if self.copy:
            X = X.copy()
        # Center data
        self.mean_ = np.mean(X, axis=0)
        X -= self.mean_
        # TODO: logging
        print 'computing zca'
        t1 = time.time()
        eigs, eigv = linalg.eigh(np.dot(X.T, X) / X.shape[0] + self.filter_bias * np.identity(X.shape[1]))
        t2 = time.time()
        print "cov estimate + eigh took",t2-t1,"seconds"
        assert not np.any(np.isnan(eigs))
        assert not np.any(np.isnan(eigv))
        assert eigs.min() > 0
        if self.n_components:
            eigs = eigs[:self.n_components]
            eigv = eigv[:, :self.n_components]
        if self.n_drop_components:
            eigs = eigs[self.n_drop_components:]
            eigv = eigv[:, self.n_drop_components:]
        self.P_ = np.dot(eigv * np.sqrt(1.0 / eigs),
                         eigv.T)
        # print 'zca components'
        # print np.square(self.P_).sum(axis=0)
        assert not np.any(np.isnan(self.P_))
        self.has_fit_ = True

    def apply(self, dataset, can_fit=False):
        X = dataset.get_design_matrix()
        assert X.dtype in ['float32', 'float64']
        if not self.has_fit_:
            assert can_fit
            self.fit(X)
        if X.dtype == 'float32': self.P_ = self.P_.astype('float32')
        new_X = np.dot(X - self.mean_, self.P_)
        dataset.set_design_matrix(new_X)

    def invert(self):
        """
        Do any necessary prep work to be able to support the "inverse" method
        later.
        """
        self.inv_P_ = np.linalg.inv(self.P_)

    def inverse(self, X):
        assert X.ndim == 2
        return np.dot(X, self.inv_P_) + self.mean_
