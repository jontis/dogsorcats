#buildTrainSet
from __future__ import division
import os, glob, numpy, pickle
#from progressbar import ProgressBar, Percentage, Bar, RotatingMarker, ETA
from joblib import Parallel, delayed, Memory
import joblib 
from scipy import ndimage
from pylearn2.datasets import dense_design_matrix, preprocessing
from math import floor, ceil
import my_zca
import matplotlib.pyplot as pyplot

#from sklearn.preprocessing import StandardScaler

# supress warning message that sklearn causes in pandas
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning, module="pandas", lineno=570)
warnings.filterwarnings("ignore", category=UserWarning, module="scipy", lineno=532) # ignore warning about changed interface


pic_cfg = {'pix_base': 50,
           'greyscale': True}
           
expand_set_cfg = {'mirror': True, # good response from zoom, rotate and mirror
                  'shift': False,
                  'shift_pix': 5,
                  'rotate': True,
                  'rot_degrees': 10,
                  'zooms' : True,
                  'zoom_ladder': [.9, 1.1]}

data_path_gen = '/home/jonathan/kaggle/dogscats/generatedData/'
data_path_source = '/home/jonathan/kaggle/dogscats/sourceData/train/'
data_path_test = '/home/jonathan/kaggle/dogscats/sourceData/test1/'
cache = Memory(cachedir=data_path_gen, compress=False)

def PlotImageList(list_images):
    units = int(ceil(((len(list_images) /3*2)**.5)/2))
    nrows, ncols = 2 * units, 3 * units
    fig, axes = pyplot.subplots(nrows=nrows, ncols=ncols, figsize=(9, 6))
    pyplot.gray()
    
    fig.suptitle('Images', fontsize=15)
    
    axes[0][0].axis('off')
    
    # Plot original images
    for count, image in enumerate(list_images):
        row = count // ncols
        col = count - row * ncols
        ax = axes[row][col]
        ax.imshow(image)
        ax.set_title(count)
        ax.axis('off')
    pyplot.show()


def GetSourceDataFileNames(pic_type):
    def FileNumber(file_path):
        return int(os.path.splitext(os.path.split(file_path)[1])[0])
    if pic_type == 'sub':
        list_file_paths = glob.glob(data_path_test + '*.jpg')
        return numpy.array(sorted(list_file_paths, key=FileNumber))
    else:
        return numpy.array(sorted(glob.glob(data_path_source + pic_type + '*.jpg')))


def ExpandTrainingSet(npa_image, verbose=False):
    list_npa_images = [npa_image]
    degrees = expand_set_cfg['rot_degrees']
    shift = expand_set_cfg['shift_pix']
    # rotate
    if expand_set_cfg['rotate']:
        loop_list = list(list_npa_images) # list() for copy, not just pointer
        for count, i in enumerate(loop_list):
            if verbose: print 'rotating ' + str(count)
            list_npa_images.append(ndimage.rotate(i, -degrees, reshape=False))
            list_npa_images.append(ndimage.rotate(i, degrees, reshape=False))
    # zoom
    if expand_set_cfg['zooms']:
        loop_list = list(list_npa_images) # list() for copy, not just pointer
        for count, i in enumerate(loop_list):
            for dount, j in enumerate(expand_set_cfg['zoom_ladder']):
                if verbose: print 'zooming ' + str(count) + ', ' + str(dount)
                npa_zoom = ndimage.zoom(i, [j, j, 1])
                if npa_zoom.shape < npa_image.shape: # zero pad up to correct size
                    diff_shape = (npa_image.shape[0] - npa_zoom.shape[0])/2, (npa_image.shape[1] - npa_zoom.shape[1])/2
                    pad_shape = (floor(diff_shape[0]), ceil(diff_shape[0])), (floor(diff_shape[1]), ceil(diff_shape[1])), (0, 0)
                    npa_zoom = numpy.pad(npa_zoom, pad_shape, 'constant')
                list_npa_images.append(npa_zoom)

    # shift
    if expand_set_cfg['shift']:
        loop_list = list(list_npa_images) # list() for copy, not just pointer
        for count, i in enumerate(loop_list):
            if verbose: print 'shifting ' + str(count)
            neg_h_shift = numpy.hstack([i[:, -shift:], i[:, :-shift]])
            pos_h_shift = numpy.hstack([i[:, shift:], i[:, :shift]])
            list_npa_images.append(numpy.vstack([neg_h_shift[-shift:], neg_h_shift[:-shift]]))
            list_npa_images.append(numpy.vstack([neg_h_shift[shift:], neg_h_shift[:shift]]))
            list_npa_images.append(numpy.vstack([pos_h_shift[-shift:], pos_h_shift[:-shift]]))
            list_npa_images.append(numpy.vstack([pos_h_shift[shift:], pos_h_shift[:shift]]))
    # mirror
    if expand_set_cfg['mirror']:
        loop_list = list(list_npa_images) # list() for copy, not just pointer
        for count, i in enumerate(loop_list):
            if verbose: print 'mirroring ' + str(count)
            list_npa_images.append(numpy.fliplr(i))
    return list_npa_images


def ReadPicture(name_file_pic, set_type):
    pix_base = pic_cfg['pix_base']
    color_channels = 3 if  pic_cfg['greyscale']==False else 1
    npa_image = ndimage.imread(name_file_pic, flatten=pic_cfg['greyscale'], mode=None) # flatten = Grayscale
    if pic_cfg['greyscale']: npa_image = numpy.expand_dims(npa_image, 3) # make sure greyscale works as color in transforms
    pix_min_side = min(npa_image.shape[:2])    
    zoom = pix_base / pix_min_side
    zoom_x = pix_base / npa_image.shape[0]
    zoom_y = pix_base / npa_image.shape[1]
    
    # zoom to base size + 10 %
    npa_image = ndimage.zoom(npa_image, [zoom_x, zoom_y, 1])
    
    # expand train set
    if set_type == 'train':
        list_npa_images = ExpandTrainingSet(npa_image)
    else:
        list_npa_images = [npa_image]
    
    # crop pictures down to size
    list_npa_images_crop = []
    for i in list_npa_images:
        start_x = max(0, (i.shape[0] - pix_base) // 2)
        start_y = max(0, (i.shape[1] - pix_base) // 2)
        temp = i[start_x: start_x + pix_base, start_y: start_y + pix_base]
        assert temp.shape == (pix_base, pix_base, color_channels), 'shape is: ' + str(temp.shape)
        list_npa_images_crop.append(i[start_x: start_x + pix_base, start_y: start_y + pix_base])
    
    # flatten and stack
    list_npa_images = []
    for i in list_npa_images_crop:
        list_npa_images.append(i.flatten())
    npa_images = numpy.row_stack(list_npa_images)
  
    return npa_images


def ReadPicturesParallell(pic_type, n_train, n_test):
    list_names_pics = GetSourceDataFileNames(pic_type)
    index_pic = numpy.random.permutation(numpy.arange(len(list_names_pics)))
    if pic_type == 'sub':
        lists_names_pics = [list_names_pics]
        set_types = ['sub']
    else:
        lists_names_pics = [list_names_pics[index_pic[:n_train]], list_names_pics[index_pic[n_train:n_train+n_test]]]
        set_types = ['train', 'test']
    
    out_list_npa_pics = []
    for set_type, i in zip(set_types, lists_names_pics):
        list_npa_pics = Parallel(n_jobs=-1, verbose=5)( # parallell loop
            delayed(ReadPicture)(
            name_file_pic, set_type)
            for name_file_pic in i)
        out_list_npa_pics += [numpy.row_stack(list_npa_pics)]

    return out_list_npa_pics, lists_names_pics


def GetDataSet(which_set='train', force_regen=False):
    n_train = 10000
    n_test = 2500

    def CheckForPrebuiltFile(path):
        if len(glob.glob(path)) > 0:
            print 'found prebuilt file ' + path + ', skipping build'
            return True

    
    def BuildNewFile():
        npa_pics_cat, names_files_cat = ReadPicturesParallell('cat', n_train, n_test)
        labels_pics_cat = [numpy.zeros(npa_pics_cat[0].shape[0]), numpy.zeros(npa_pics_cat[1].shape[0])]
        npa_pics_dog, names_files_dog = ReadPicturesParallell('dog', n_train, n_test)
        labels_pics_dog = [numpy.ones(npa_pics_dog[0].shape[0]), numpy.ones(npa_pics_dog[1].shape[0])]

        for i, j in enumerate(['train', 'test']):
            npa_pics = numpy.row_stack([npa_pics_cat[i], npa_pics_dog[i]])
            labels_pics = numpy.append(labels_pics_cat[i], labels_pics_dog[i])

            # reorder dataset so examples of each label is in each batch
            index_reorder = numpy.random.permutation(npa_pics.shape[0])
            npa_pics = npa_pics[index_reorder,:]
            labels_pics = labels_pics[index_reorder]

            print j
            name_file_data = j + '_data.npy'
            name_file_labels = j + '_labels.npy'

            print 'saving ' + name_file_data
            numpy.save(data_path_gen + name_file_data, npa_pics)
            numpy.save(data_path_gen + name_file_labels, labels_pics)
        
        # submission set
        npa_pics_sub, names_files_sub = ReadPicturesParallell('sub', n_train, n_test)
        name_file_data = 'sub_data.npy'
        name_file_labels = 'sub_labels.npy'
        #name_file_ids = 'sub_ids.npy'
        print 'saving ' + name_file_data
        numpy.save(data_path_gen + name_file_data, npa_pics_sub[0])
        #numpy.save(data_path_gen + name_file_ids, ids_pics)
        numpy.save(data_path_gen + name_file_labels, numpy.array(0))

    
    def CheckDataFilesAndParse(which_set='train'):
        # check for presence of the files, reparse if there are none
        name_file_data = which_set + '_data.npy'
        name_file_labels = which_set + '_labels.npy'
        
        if  not force_regen:
            if CheckForPrebuiltFile(data_path_gen + name_file_data):
                return (numpy.load(data_path_gen + name_file_data),
                        numpy.load(data_path_gen + name_file_labels))

        # building new set
        BuildNewFile()
        return (numpy.load(data_path_gen + name_file_data),
                numpy.load(data_path_gen + name_file_labels))
                
    npa_pics, labels_pics = CheckDataFilesAndParse(which_set)
    return npa_pics, labels_pics


@cache.cache
def ZcaTrain(dataset):
    if (len(glob.glob(data_path_gen + 'pickled_zca.pkl')) > 0): # prefit zca
        zca = joblib.load(data_path_gen + 'pickled_zca.pkl')
        #file_zca = open(data_path_gen + 'pickled_zca.pkl', 'rb')
        #zca = pickle.load(file_zca)
    else:
        zca = my_zca.ZCA()
        zca.fit(dataset.X) 
        print 'zca fit finished'
        joblib.dump(zca, data_path_gen + 'pickled_zca.pkl', compress=0, cache_size=100)
        #file_zca = open(data_path_gen + 'pickled_zca.pkl', 'wb')
        #pickle.dump(zca, file_zca)
        #file_zca.close()
    print 'starting zca train apply'
    zca.apply(dataset)
    print 'done zca apply'
    del zca
    return dataset


@cache.cache
def ZcaTest(dataset):
    zca = joblib.load(data_path_gen + 'pickled_zca.pkl')
    #file_zca = open(data_path_gen + 'pickled_zca.pkl', 'rb')
    #zca = pickle.load(file_zca)
    print 'starting zca test apply'
    zca.apply(dataset)
    print 'done zca apply'
    del zca    
    #file_zca.close()
    return dataset


@cache.cache
def ZcaSub(dataset):
    zca = joblib.load(data_path_gen + 'pickled_zca.pkl')
    #file_zca = open(data_path_gen + 'pickled_zca.pkl', 'rb')
    #zca = pickle.load(file_zca)
    print 'starting zca sub apply'
    zca.apply(dataset)
    print 'done zca apply'
    del zca    
    #file_zca.close()
    return dataset


def ZcaHelper(which_set, dataset):
    if which_set == 'train': return ZcaTrain(dataset)
    if which_set == 'test': return ZcaTest(dataset)
    if which_set == 'sub': return ZcaSub(dataset)


class catsDogsDataSet(dense_design_matrix.DenseDesignMatrix):
    def __init__(self, which_set='train', axes = ('b', 0, 1, 'c')):
        color_channels = 3 if pic_cfg['greyscale']==False else 1
           
        X, y = GetDataSet(which_set)
        X = X.astype('float32')
        
        if which_set != 'sub':
            one_hot = numpy.zeros((len(y), len(set(y))))
            for i in xrange(len(y)): one_hot[i, y[i]] = 1
            y = one_hot
        else:
            y = None
        
        #scale and center
        X = X / 255.
        if pic_cfg['greyscale']: X = X - X.mean(axis=0) # recommended to only remove mean for greyscale, not for color

        view_converter = dense_design_matrix.DefaultViewConverter((pic_cfg['pix_base'], pic_cfg['pix_base'], color_channels), axes)

        super(catsDogsDataSet, self).__init__(X = X, y = y, view_converter = view_converter)


class catsDogsDataSetContrast(dense_design_matrix.DenseDesignMatrix):
    def __init__(self, which_set='train', axes = ('b', 0, 1, 'c')):
        
        dataset = catsDogsDataSet(which_set, axes)
        preprocessing.GlobalContrastNormalization(sqrt_bias=10, use_std=True).apply(dataset)
        
        super(catsDogsDataSetContrast, self).__init__(X = dataset.X, y = dataset.y, view_converter = dataset.view_converter)       


class catsDogsDataSetContrastZca(dense_design_matrix.DenseDesignMatrix):
    def __init__(self, which_set='train', axes = ('b', 0, 1, 'c')):
        
        dataset = catsDogsDataSet(which_set, axes)
        preprocessing.GlobalContrastNormalization(sqrt_bias=10, use_std=True).apply(dataset)
        
        dataset = ZcaHelper(which_set, dataset)
        
        super(catsDogsDataSetContrastZca, self).__init__(X = dataset.X, y = dataset.y, view_converter = dataset.view_converter)       





