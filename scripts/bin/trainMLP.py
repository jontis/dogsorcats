from pylearn2.config import yaml_parse
import logging

logging.basicConfig(filename='run.log', filemode='w', level=logging.DEBUG)
log = logging.getLogger('pylearn2')

def Train(file_yaml):
    mlp_yaml = open(file_yaml, 'r').read()
    
    train = yaml_parse.load(mlp_yaml)
    train.main_loop()
