import sys
from pylearn2.config import yaml_parse

sys.path.append('/home/jonathan/kaggle/dogscats/scripts/bin')


layer1_yaml = open('dae_l1.yaml', 'r').read()
train = yaml_parse.load(layer1_yaml)
train.main_loop()