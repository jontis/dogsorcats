import sys, os, pylab, pandas
from pylearn2.utils import serial
from pylearn2.config import yaml_parse
import catsDogsDataSet

data_path_gen = '/home/jonathan/kaggle/dogscats/generatedData/'


#hardcode
model = serial.load('4.best.pkl' )
sub_data = catsDogsDataSet.catsDogsDataSetContrastZca(which_set='sub', axes=['c', 0, 1, 'b'])

# use smallish batches to avoid running out of memory
batch_size = 100
model.set_batch_size(batch_size)

# dataset must be multiple of batch size of some batches will have
# different sizes. theano convolution requires a hard-coded batch size
m = sub_data.X.shape[0]
extra = batch_size - m % batch_size
assert (m + extra) % batch_size == 0
import numpy as np
if extra > 0:
	sub_data.X = np.concatenate((sub_data.X, np.zeros((extra, sub_data.X.shape[1]),
	dtype=sub_data.X.dtype)), axis=0)
assert sub_data.X.shape[0] % batch_size == 0


X = model.get_input_space().make_batch_theano()
Y = model.fprop(X)

from theano import tensor as T

y = T.argmax(Y, axis=1)

from theano import function

f = function([X], y)


y = []

for i in xrange(sub_data.X.shape[0] / batch_size):
	print str(i)
	x_arg = sub_data.X[i*batch_size:(i+1)*batch_size,:]
	if X.ndim > 2:
		x_arg = sub_data.get_topological_view(x_arg)
	y.append(f(x_arg.astype(X.dtype)))

y = np.concatenate(y)
assert y.ndim == 1
assert y.shape[0] == sub_data.X.shape[0]
# discard any zero-padding that was used to give the batches uniform size
y = y[:m]

bool_image = np.zeros(( y.shape[0], max(y)+1))
for i in xrange(max(y)):
	bool_image[:, i] = (y == i)

pylab.imshow(bool_image, aspect='auto')
pylab.show()

# print to submission file
ids = np.arange(len(y))+1
df_sub = pandas.DataFrame({'id': ids, 'label': y})
df_sub.to_csv(data_path_gen + 'sub_labels.csv', index=False)

# read in hand labels and compare
df_hand = pandas.read_csv(data_path_gen + 'hand_labels.csv')
npa_truth = np.logical_xor(df_sub[:100]['label'], df_hand['label'])
truth_ratio = 1 - sum(npa_truth)/ float(len(npa_truth))
print 'truth ratio: ' + str(truth_ratio)
